// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QMenu>
#include <QMessageBox>
#include <QToolButton>
#include <QToolBar>

#include <qwt_legend.h>
#include <qwt_plot_zoomer.h>
#include <qwt_plot_panner.h>
#include <qwt_picker_machine.h>
#include <qwt_plot_renderer.h>

#include "OpenMeca/Gui/MainPlotWindow.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Item/Physical.hpp"
#include "OpenMeca/Setting/Simulation.hpp"
#include "OpenMeca/Util/Dimension.hpp"



class Zoomer: public QwtPlotZoomer
{
public:
    Zoomer( int xAxis, int yAxis, QWidget *canvas ):
        QwtPlotZoomer( xAxis, yAxis, canvas )
    {
        setTrackerMode( QwtPicker::AlwaysOff );
        setRubberBand( QwtPicker::NoRubberBand );

        // RightButton: zoom out by 1
        // Ctrl+RightButton: zoom out to full size

        setMousePattern( QwtEventPattern::MouseSelect2,
            Qt::RightButton, Qt::ControlModifier );
        setMousePattern( QwtEventPattern::MouseSelect3,
            Qt::RightButton );
    }
};



namespace OpenMeca
{
  namespace Gui
  {
  

    MainPlotWindow* MainPlotWindow::me_ = 0;

    MainPlotWindow& 
    MainPlotWindow::Get()
    {
      OMC_ASSERT_MSG(me_!=0, "The singleton pointer is null");
      return *me_;
    }

    MainPlotWindow::MainPlotWindow(QWidget * parent)
      :QMainWindow(parent),
       time_(Core::SystemSettingT<Setting::Simulation>::Get().GetTimeArray()),
       data_(),
       currentUnitPtr_(0)
    {
      OMC_ASSERT_MSG(me_ == 0, "The singleton pointer is not null");
      setupUi(this);
      me_ = this;

      const int icon_size = Gui::MainWindow::Get().GetIconSize();
      
      tree_->setIconSize(QSize(icon_size,icon_size));
      tree_->setAutoScrollMargin(icon_size);
      tree_->headerItem()->setText(0, tr(""));
      tree_->headerItem()->setText(1, tr("Value"));
      tree_->headerItem()->setText(2, tr("Show/Hide"));
      
      // pop-up menu when right-click
      plot_->setContextMenuPolicy(Qt::CustomContextMenu);
      plot_->setCanvasBackground(QColor("MidnightBlue"));
      // legend
      QwtLegend *legend = new QwtLegend;
      legend->setFrameStyle(QFrame::Box|QFrame::Sunken);
      plot_->insertLegend(legend, QwtPlot::BottomLegend);
      
      // axis
      plot_->setAxisTitle(QwtPlot::xBottom, "Time (s)");
      

      // Signal & slots
      Setting::Simulation* simu = &Core::SystemSettingT<Setting::Simulation>::Get();
      QObject::connect(actionClear_, SIGNAL(triggered()), this, SLOT(Clear()));
      QObject::connect(simu, SIGNAL(StepChanged(OpenMeca::Setting::Simulation::Step)), 
		       this, SLOT(Update(OpenMeca::Setting::Simulation::Step)));


      // It comes from the bode example from Qwt  
      zoomer_[0] = new Zoomer(QwtPlot::xBottom, QwtPlot::yLeft, plot_->canvas());
      zoomer_[0]->setRubberBand( QwtPicker::RectRubberBand);
      zoomer_[0]->setRubberBandPen(QColor(Qt::green));
      zoomer_[0]->setTrackerMode(QwtPicker::ActiveOnly);
      zoomer_[0]->setTrackerPen(QColor(Qt::white));
      
      zoomer_[1] = new Zoomer(QwtPlot::xTop, QwtPlot::yRight, plot_->canvas());
      
      panner_ = new QwtPlotPanner(plot_->canvas());
      panner_->setMouseButton(Qt::MidButton);
      
      picker_ = new QwtPlotPicker(QwtPlot::xBottom, QwtPlot::yLeft,
				   QwtPlotPicker::CrossRubberBand, QwtPicker::AlwaysOn,
				   plot_->canvas());
      picker_->setStateMachine(new QwtPickerDragPointMachine());
      picker_->setRubberBandPen(QColor(Qt::green));
      picker_->setRubberBand(QwtPicker::CrossRubberBand);
      picker_->setTrackerPen(QColor(Qt::white));

      connect(actionZoom_, SIGNAL(toggled(bool)), SLOT(EnableZoomMode(bool)));
      connect(actionPdf_,  SIGNAL(triggered()), SLOT(ExportToPDF()));

      EnableZoomMode(false);
    }
    
    MainPlotWindow::~MainPlotWindow()
    {
    }
    
    bool
    MainPlotWindow::SetUnit(const Util::Unit& unit)
    {
      if (currentUnitPtr_ == 0)
	{
	  currentUnitPtr_ = &unit;
	  QString ylabel = unit.GetDimension().GetName().c_str();
	  ylabel += " (" + QString(unit.GetSymbol().c_str()) + " )";
	  plot_->setAxisTitle(QwtPlot::yLeft, ylabel);
	  return true;
	}
      else if (currentUnitPtr_ != &unit)
	{
	  const std::string msg = "You want to add a data with a different unit from the current displayed data on the graph. Are you sure ?";

	  int ret = QMessageBox::warning(this, tr("openmeca (plot)"),
					 tr(msg.c_str()),
					 QMessageBox::YesRole|QMessageBox::NoRole,
					 QMessageBox::NoRole);
	  if (ret==QMessageBox::NoRole)
	    {
	      plot_->setAxisTitle(QwtPlot::yLeft, "?");
	      return true;
	    }
	  return false;
	}
      return true;
    }

    void
    MainPlotWindow::showEvent(QShowEvent * event)
    {
      QMainWindow::showEvent(event);
      UpdateDataTree();
    }

    void MainPlotWindow::Update(OpenMeca::Setting::Simulation::Step)
    {
      if (isHidden()==true)
	return;
      
      std::map<QwtPlotCurve*, const std::vector<double>*>::iterator it;
      for (it=data_.begin(); it!=data_.end(); ++it)
	{
	  QwtPlotCurve* curve =it->first;
	  const std::vector<double>& data = *it->second;
	  SetSample(data, curve);
	}
      plot_->replot();

      // update tree view
      Core::SetOfBase<Item::Physical>& physicals = Core::System::Get().GetSetOf<Item::Physical>();
      
      for (unsigned int i = 0; i < physicals.GetTotItemNumber(); ++i)
	{
	  Item::Physical& phys = physicals(i);
	  phys.UpdateDataTree();
	}
    }

    void
    MainPlotWindow::UpdateDataTree()
    {
      tree_->clear();
      Core::SetOfBase<Item::Physical>& physicals = 
	Core::System::Get().GetSetOf<Item::Physical>();
      
      for (unsigned int i = 0; i < physicals.GetTotItemNumber(); ++i)
	{
	  Item::Physical& phys = physicals(i);
	  const QString& title = phys.GetName().c_str();
	  const QIcon& icon = phys.GetIcon();

	  QTreeWidgetItem* treeItem = new QTreeWidgetItem(tree_);
	  treeItem->setIcon(0, icon);
	  treeItem->setText(0, title);
	  phys.FillDataTree(treeItem);
	}

      
      

    }

    void
    MainPlotWindow::AddData(const std::vector<double>& data, 
			    const QString& label, 
			    const Util::Unit& unit)
    {
      if (SetUnit(unit))
	{
	  QwtPlotCurve* curve = new QwtPlotCurve(label); 
	  SetSample(data, curve);
	  curve->attach(plot_);
	  curve->setPen(* new QPen(Util::Color().GetQColor(),2, Qt::SolidLine));    
	  plot_->replot();
	  OMC_ASSERT_MSG(data_.count(curve) == 0, "The curve is already registered");
	  data_[curve] = &data;
	}
    }

    
    void
    MainPlotWindow::RemoveData(const std::vector<double>& data)
    {
      QwtPlotCurve* curve = nullptr;
      for (auto& d: data_)
	if (d.second == &data)
	  curve = d.first;

      OMC_ASSERT_MSG(curve != 0, "Can't find the data to remove");
      data_.erase(curve);
      delete(curve);
      
      if (data_.size() == 0)
	currentUnitPtr_ = 0;

      plot_->replot();
    }

    void
    MainPlotWindow::Clear()
    {
      plot_->detachItems();
      data_.clear();
      plot_->setAxisTitle(QwtPlot::yLeft, "");
      currentUnitPtr_ = 0;
      plot_->replot();
      UpdateDataTree();
    }
    
    

    
    void MainPlotWindow::SetSample(const std::vector<double>& data, 
				   QwtPlotCurve* curve)
    {

      // OMC_ASSERT_MSG(data.size() == time_.size() || data.size() + 1 == time_.size(),
      // 		     "The number of data point is incoherent");

      int datasize = data.size();
      if (datasize > int(time_.size()))
	datasize =  time_.size();

      curve->setSamples(&time_[0], &data[0], datasize);
    }

  
    void MainPlotWindow::ExportToPDF()
    {
      QwtPlotRenderer renderer;
      renderer.exportTo(plot_, "myplot.pdf");
    }


    void MainPlotWindow::EnableZoomMode(bool on)
    {
      if (on==true)
	{
	  zoomer_[0]->setZoomBase(true);
	  zoomer_[1]->setZoomBase(true);
	}
      
      panner_->setEnabled(on);
      zoomer_[0]->setEnabled(on);
      
      zoomer_[0]->zoom(0);
      zoomer_[1]->setEnabled(on);
      zoomer_[1]->zoom(0);
      picker_->setEnabled(true);
    }
    

  }
}
