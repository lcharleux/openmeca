// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QMouseEvent>

#include "OpenMeca/Gui/Viewer.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/SelectionManager.hpp"
#include "OpenMeca/Core/DrawableUserItem.hpp"
#include "OpenMeca/Core/CommonProperty.hpp"
#include "OpenMeca/Setting/Simulation.hpp"

namespace OpenMeca
{
  namespace Gui
  {
  

    Viewer::Viewer(QWidget * parent)
      :QGLViewer(parent),
       rightClick_(false),
       snapshot_(false),
       drawAxis_(false)
    {
    }
    
    Viewer::~Viewer()
    {
    }

    void 
    Viewer::SetSnapShotMode(bool mode)
    {
      snapshot_ = mode;
    }

    void 
    Viewer::draw()
    {
      //glPolygonMode(GL_FRONT,GL_LINE);
      if (drawAxis_)
	Geom::Frame<_3D>::Global.Draw();
            
      Core::System::Get().Draw();
    }


    void 
    Viewer::drawWithNames()
    {
      Core::System::Get().DrawWithNames();
    }

    void Viewer::postDraw()
    {
      QGLViewer::postDraw();
      DrawCornerAxis();
    }


    void 
    Viewer::mousePressEvent(QMouseEvent* e)
    {
      Core::Singleton<Core::SelectionManager>::Get().SetNoItemSelected();
      if (e->button() == Qt::RightButton)
	rightClick_ = true;
      else
	rightClick_ = false;


      QGLViewer::mousePressEvent(e);
    }

    void 
    Viewer::postSelection(const QPoint& p)
    {
      if (selectedName() == -1)
	Core::Singleton<Core::SelectionManager>::Get().SetNoItemSelected();
      else
	{
	  Core::Singleton<Core::SelectionManager>::Get().SetDrawableItemSelected(selectedName());
	  Core::UserItem& item = Core::DrawableUserItem::GetDrawableItemByGLKey(selectedName());
	  if (rightClick_)
	    Core::CommonProperty::GetClass(item.GetStrType()).GetPopUpMenu().popup(mapToGlobal(p));
	}
    }

    void 
    Viewer::init()
    {
      setMouseBinding(Qt::RightButton + Qt::SHIFT, SELECT);
    }

    void 
    Viewer::animate()
    {
      if (snapshot_)
	saveSnapshot();
    }

    void
    Viewer::DrawAxis(bool draw)
    {
      drawAxis_ = draw;
    }

    void 
    Viewer::DrawText(const std::string& str, const Geom::Point<_3D>& p)
    {
      glDisable(GL_LIGHTING);
      glDisable(GL_DEPTH_TEST);
      glColor3f(1.0f, 1.0f, 1.0f);
      const Geom::Point<_3D> p_glob(p, &Geom::Frame<_3D>::GetGlobal);
      QGLWidget::renderText(p_glob[0], p_glob[1],p_glob[2], str.c_str());
      glEnable(GL_DEPTH_TEST);
      glEnable(GL_LIGHTING);
    }
    

    void Viewer::DrawCornerAxis()
    {

      int viewport[4];
      int scissor[4];
  
      // The viewport and the scissor are changed to fit the lower left
      // corner. Original values are saved.
      glGetIntegerv(GL_VIEWPORT, viewport);
      glGetIntegerv(GL_SCISSOR_BOX, scissor);
  
      // Axis viewport size, in pixels
      const int size = 150;
      glViewport(0,0,size,size);
      glScissor(0,0,size,size);
  
      // The Z-buffer is cleared to make the axis appear over the
      // original image.
      glClear(GL_DEPTH_BUFFER_BIT);
  
      // Tune for best line rendering
      glDisable(GL_LIGHTING);
      glLineWidth(3.0);
  
      glMatrixMode(GL_PROJECTION);
      glPushMatrix();
      glLoadIdentity();
      glOrtho(-1, 1, -1, 1, -1, 1);
  
      glMatrixMode(GL_MODELVIEW);
      glPushMatrix();
      glLoadIdentity();
      glMultMatrixd(camera()->orientation().inverse().matrix());
  
      glBegin(GL_LINES);
      glColor3f(1.0, 0.0, 0.0);
      glVertex3f(0.0, 0.0, 0.0);
      glVertex3f(1.0, 0.0, 0.0);
  
      glColor3f(0.0, 1.0, 0.0);
      glVertex3f(0.0, 0.0, 0.0);
      glVertex3f(0.0, 1.0, 0.0);
  
      glColor3f(0.0, 0.0, 1.0);
      glVertex3f(0.0, 0.0, 0.0);
      glVertex3f(0.0, 0.0, 1.0);
      glEnd();


      glMatrixMode(GL_PROJECTION);
      glPopMatrix();
  
      glMatrixMode(GL_MODELVIEW);
      glPopMatrix();
  
      glEnable(GL_LIGHTING);
  
      // The viewport and the scissor are restored.
      glScissor(scissor[0],scissor[1],scissor[2],scissor[3]);
      glViewport(viewport[0],viewport[1],viewport[2],viewport[3]);

    }



  }
}
