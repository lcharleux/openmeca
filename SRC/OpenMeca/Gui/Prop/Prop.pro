## This file is part of OpenMeca, an easy software to do mechanical simulation.
##
## Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
##
## Copyright (C) 2012 Damien ANDRE
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.



HEADERS += Gui/Prop/Prop.hpp \
           Gui/Prop/PropT.hpp \
           Gui/Prop/PropAxis.hpp \
           Gui/Prop/PropTree.hpp \
           Gui/Prop/PropString.hpp \
           Gui/Prop/PropColor.hpp \
           Gui/Prop/PropVector.hpp \
           Gui/Prop/PropDouble.hpp \
           Gui/Prop/PropEnumT.hpp \
           Gui/Prop/PropPoint.hpp \
           Gui/Prop/PropExprPoint.hpp \
           Gui/Prop/PropExprAttitude.hpp \
           Gui/Prop/PropSelectItemT.hpp \
           Gui/Prop/PropBool.hpp \
           Gui/Prop/PropMatrix.hpp \
           Gui/Prop/PropAttitude.hpp \
           Gui/Prop/PropMotionLaw.hpp \
           Gui/Prop/PropExpr.hpp 

SOURCES += Gui/Prop/Prop.cpp \
           Gui/Prop/PropTree.cpp \
           Gui/Prop/PropAxis.cpp \
           Gui/Prop/PropString.cpp \
           Gui/Prop/PropColor.cpp \
           Gui/Prop/PropVector.cpp \
           Gui/Prop/PropDouble.cpp \
           Gui/Prop/PropPoint.cpp \
           Gui/Prop/PropExprPoint.cpp \
           Gui/Prop/PropExprAttitude.cpp \
           Gui/Prop/PropBool.cpp \
           Gui/Prop/PropMatrix.cpp \
           Gui/Prop/PropAttitude.cpp \
           Gui/Prop/PropMotionLaw.cpp \
           Gui/Prop/PropExpr.cpp
