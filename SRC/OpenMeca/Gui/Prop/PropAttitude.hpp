// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Prop_PropAttitude_hpp
#define OpenMeca_Prop_PropAttitude_hpp


#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>


#include "OpenMeca/Gui/Prop/PropT.hpp"
#include "OpenMeca/Gui/Widget/WidgetDouble.hpp"
#include "OpenMeca/Util/Dimension.hpp"

#include "OpenMeca/Geom/Quaternion.hpp"
#include "OpenMeca/Geom/Point.hpp"
#include "OpenMeca/Geom/Frame.hpp"



namespace OpenMeca
{
  namespace Gui
  {
    class PropAttitude:  public PropT<Geom::Quaternion<_3D> >
    {
      public:
      PropAttitude(QWidget* parent);
      virtual ~PropAttitude();
      
      void Insert(PropTree&);

      bool Check();
      void ApplyValue();

    private:
      void Init();

    private:
      QTreeWidgetItem itemAxis_;
      QTreeWidgetItem itemX_;
      QTreeWidgetItem itemY_;
      QTreeWidgetItem itemZ_;
      QTreeWidgetItem itemA_;
      WidgetDouble x_;
      WidgetDouble y_;
      WidgetDouble z_;
      WidgetDouble a_;
    };

  }
}
#endif

