// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QApplication>
#include <iostream>

#include "OpenMeca/Gui/Prop/PropExprAttitude.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Util/Unit.hpp"

namespace OpenMeca
{
  namespace Gui
  {

    PropExprAttitude::PropExprAttitude(QWidget* parent)
      :PropT<Util::ExprAttitude>(*parent),
       itemAxis_(),
       itemX_(),
       itemY_(),
       itemZ_(),
       x_(0),
       y_(0),
       z_(0),
       a_(0)
    {
      AddSubItem(&itemAxis_);
      AddSubItem(&itemX_);
      AddSubItem(&itemY_);
      AddSubItem(&itemZ_);
      AddSubItem(&itemA_);

      x_.SetDimension(Util::Dimension::Get("Length"));
      y_.SetDimension(Util::Dimension::Get("Length"));
      z_.SetDimension(Util::Dimension::Get("Length"));
      a_.SetDimension(Util::Dimension::Get("Angle"));
    }

    PropExprAttitude::~PropExprAttitude()
    {
    }

    
    void 
    PropExprAttitude::Insert(PropTree& tree)
    {
      tree.addTopLevelItem(&item_);
      item_.setText(0, Prop::GetLabel());
      
      item_.addChild(&itemAxis_);
      itemAxis_.setText(0, "Axis");

      itemAxis_.addChild(&itemX_);
      itemX_.setText(0, "X");
      tree.setItemWidget(&itemX_,1, &x_);

      itemAxis_.addChild(&itemY_);
      itemY_.setText(0, "Y");
      tree.setItemWidget(&itemY_,1, &y_);

      itemAxis_.addChild(&itemZ_);
      itemZ_.setText(0, "Z");
      tree.setItemWidget(&itemZ_,1, &z_);

      item_.addChild(&itemA_);
      itemA_.setText(0, "Angle");
      tree.setItemWidget(&itemA_,1, &a_);
    }



    void 
    PropExprAttitude::Init()
    {
      Util::ExprAttitude& p = PropT<Util::ExprAttitude >::GetValue();    
      x_.SetExpr(p.GetAxis().GetExpressionX());
      y_.SetExpr(p.GetAxis().GetExpressionY());
      z_.SetExpr(p.GetAxis().GetExpressionZ());
      a_.SetExpr(p.GetAngle().GetExpression());
    }
    

   
    bool
    PropExprAttitude::Check()
    {
      Util::ExprAttitude& p = PropT<Util::ExprAttitude >::GetCopy();
      Util::Expr& x = p.GetAxis().GetExpressionX();
      Util::Expr& y = p.GetAxis().GetExpressionY();
      Util::Expr& z = p.GetAxis().GetExpressionZ();
      Util::Expr& a = p.GetAngle().GetExpression();

      if (x_.GetExpr(x) && y_.GetExpr(y) && z_.GetExpr(z) && a_.GetExpr(a))
	{
	  if (x()==0 && y()==0 && z()==0.)
	    {
	      QString msg(QObject::tr("The axis can't be null"));
	      x_.DisplayHelp(msg);
	      return false;
	    }
	  return true;
	}
      return false;
    }

    void     
    PropExprAttitude::PostChangement()
    {   
      Util::ExprAttitude& q = PropT<Util::ExprAttitude >::GetValue();
      q.Update();
    }


  }
}


