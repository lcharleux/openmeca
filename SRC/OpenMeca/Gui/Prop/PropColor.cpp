// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <QColorDialog>
#include <QPalette>

#include "OpenMeca/Gui/Prop/PropColor.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    PropColor::PropColor(QWidget* parent)
      :PropT<Util::Color>(*parent),
       pushButton_()
    {
      QObject::connect(&pushButton_, SIGNAL( clicked() ), this, SLOT( NewPropColor() ) );
      pushButton_.setMaximumHeight(15);
    }
  
    PropColor::~PropColor()
    {
    }

    void
    PropColor::Insert(PropTree& tree)
    {
      tree.addTopLevelItem(&item_);
      item_.setText(0, GetLabel());
      tree.setItemWidget(&item_,1, &pushButton_);
      
    }

    void
    PropColor::Init()
    {
      UpdateBackground();
    }

    bool 
    PropColor::Check()
    {
      return true;
    }

 
    void 
    PropColor::NewPropColor()
    {
      QColor newColor = QColorDialog::getColor();
      if (newColor.isValid())
	{
	  GetCopy()=Util::Color(newColor);
	  UpdateBackground();
	}
     
    }


    void 
    PropColor::UpdateBackground()
    {
      QPalette palette = pushButton_.palette();
      QColor color = GetCopy().GetQColor();
      palette.setColor(QPalette::Button, color);
      pushButton_.setPalette(palette);
    }

  }
}
