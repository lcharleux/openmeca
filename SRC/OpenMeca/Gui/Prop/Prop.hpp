// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_Prop_Prop_hpp
#define OpenMeca_Gui_Prop_Prop_hpp

#include <QWidget>
#include <QTreeWidgetItem>
#include <QColor>
#include <QToolTip>

#include "OpenMeca/Core/AutoRegister.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    class Dialog;
    class PropTree;
    
    class Prop
    {
      
    public:
      Prop(QWidget& parent);
      virtual ~Prop();
      virtual bool Check()=0;  
      virtual void ApplyChangement() = 0;
      virtual void CancelChangement() = 0;
      virtual void Reset() = 0;
      virtual void Insert(PropTree&){assert(0);};
      Dialog& GetParent();
      void ParentDeleted();
      void DisplayHelp(const std::string& message); //Assert here
      void SetLabel(const QString&);
      QString GetLabel() const;
      QTreeWidgetItem& GetTreeWidgetItem();
      void SetColor(const QColor&);

    private:
      bool CouldCastToDialog(QObject* widget);

    protected:
      void AddSubItem(QTreeWidgetItem*);
      
    protected:
      QTreeWidgetItem item_;
      std::vector<QTreeWidgetItem*> subItem_;
      QColor color_;

    private:
      QString label_;
      Dialog* parent_;
      bool parentDeleted_;
    };

  }
}
#endif
