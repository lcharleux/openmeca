// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Prop_PropAxis_hpp
#define OpenMeca_Prop_PropAxis_hpp


#include <QLabel>
#include <QLineEdit>

#include "OpenMeca/Gui/Prop/PropT.hpp"
#include "OpenMeca/Gui/Widget/WidgetDouble.hpp"
#include "OpenMeca/Util/Dimension.hpp"

#include "OpenMeca/Geom/Vector.hpp"
#include "OpenMeca/Geom/Quaternion.hpp"
#include "OpenMeca/Geom/Frame.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    class PropAxis: public PropT<Geom::Vector<_3D> >
    {
      public:
      PropAxis(QWidget* parent);
      virtual ~PropAxis();

      void Insert(PropTree&);
      
      void SetDimension(const Util::Dimension& dim);
      const Util::Dimension& GetDimension() const;
      
      bool Check();
      void ApplyValue();


    private:
      void Init();

    private:
      QTreeWidgetItem itemX_;
      QTreeWidgetItem itemY_;
      QTreeWidgetItem itemZ_;
      WidgetDouble x_;
      WidgetDouble y_;
      WidgetDouble z_;
 
   
    };

  }
}
#endif

