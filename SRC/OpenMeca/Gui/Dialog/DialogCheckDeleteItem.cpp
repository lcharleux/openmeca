// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <iostream>
#include <QListWidgetItem>

#include "OpenMeca/Core/UserItem.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Gui/Dialog/DialogCheckDeleteItem.hpp"

namespace OpenMeca
{
  namespace Gui
  {

    DialogCheckDeleteItem* DialogCheckDeleteItem::me_ = 0;

    DialogCheckDeleteItem& 
    DialogCheckDeleteItem::Get()
    {
      if (me_ == 0)
	{
	  new DialogCheckDeleteItem();
	}
      return *me_;
    }


  
    DialogCheckDeleteItem::DialogCheckDeleteItem()
      :QDialog(&MainWindow::Get())
    {
      OMC_ASSERT_MSG(me_ == 0, "The singleton is null");
      Ui::DialogCheckDeleteItem::setupUi(this);
      setModal(true);
      me_ = this;
    }

    
    DialogCheckDeleteItem::~DialogCheckDeleteItem()
    {
      me_ = 0;
    }


    void 
    DialogCheckDeleteItem::BuildListView(const Core::SetOfBase<Core::UserItem>& childs)
    {
      listWidget_->clear();
      typename std::list<Core::UserItem *>::const_iterator it;
      for ( it=childs.Begin(); it != childs.End(); it++ )
	{
	  new QListWidgetItem((*it)->GetIcon(), (*it)->GetName().c_str(), listWidget_);
	}
    }
  }
}
