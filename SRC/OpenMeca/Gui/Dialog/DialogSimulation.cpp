// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <iostream>


#include "OpenMeca/Setting/Simulation.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Gui/Dialog/DialogSimulation.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    DialogSimulation::DialogSimulation()
      :DialogSystemSettingT<Setting::Simulation>(),
      step_(this),
      time_(this),
      solver_(this),
      integrationType_(this),
      animationPeriod_(this),
      staticEquilibrium_(this)
    {
      Ui::DialogSimulation::setupUi(this);
      QObject::connect(player_, SIGNAL(Played()) , this, SLOT(Start()));
      QObject::connect(player_, SIGNAL(Stopped()), this, SLOT(Stop()));
      QObject::connect(player_, SIGNAL(Paused()) , this, SLOT(Pause()));
      QObject::connect(close_ , SIGNAL(clicked()), this, SLOT(Close()));

      step_.SetDimension(Util::Dimension::Get("Time"));
      step_.SetLabel(QObject::tr("time step"));
      step_.AddCondition(new Core::Superior<double>(0.));

      time_.SetDimension(Util::Dimension::Get("Time"));
      time_.SetLabel(QObject::tr("total simulation time"));
      time_.AddCondition(new Core::Superior<double>(0.));

      solver_.Prop::SetLabel(QObject::tr("Solver"));
      integrationType_.Prop::SetLabel(QObject::tr("Integration"));
      
      animationPeriod_.SetDimension(Util::Dimension::Get("Time"));
      animationPeriod_.Prop::SetLabel(QObject::tr("Animation period"));
      animationPeriod_.AddCondition(new Core::Superior<double>(0.));

      staticEquilibrium_.Prop::SetLabel(QObject::tr("Static equilibrium"));
      
      
      GetPropTree().Add(step_);
      GetPropTree().Add(time_);
      GetPropTree().Add(solver_);
      GetPropTree().Add(integrationType_);
      GetPropTree().Add(animationPeriod_);
      GetPropTree().Add(staticEquilibrium_);
    }

    DialogSimulation::~DialogSimulation()
    {
      
    }

    void
    DialogSimulation::Init()
    {
      step_.SetValue(GetSetting().GetTimeStep());
      time_.SetValue(GetSetting().GetTotalSimulationTime());
      solver_.SetValue(GetSetting().GetSolver());
      integrationType_.SetValue(GetSetting().GetIntegrationType());
      animationPeriod_.SetValue(GetSetting().GetAnimationPeriod());
      staticEquilibrium_.SetValue(GetSetting().GetStaticEquilibrium());
	
      step_.AddCondition(new Core::Superior<double>(0.));
      time_.AddCondition(new Core::Superior<double>(0.));
      animationPeriod_.AddCondition(new Core::Superior<double>(0.));

    }


    bool
    DialogSimulation::Check()
    {
      if (!Dialog::Check())
	return false;
      
      const double stepValue = step_.GetCopy();
      const double timeValue = time_.GetCopy();
    
      if (stepValue > timeValue)
	{
	  OMC_WARNING_MSG(tr("The time step must be lower than the total simulation time"));
	  return false;
	}

      
      return true;
    }

    void 
    DialogSimulation::Stop()
    {
      GetSetting().Stop();
    }

    void 
    DialogSimulation::Start()
    {
      if (Check())
	{
	  ApplyChangement();
	  GetSetting().Start();
	}
      
    }

    void 
    DialogSimulation::Pause()
    {
      if (Check())
	{
	  ApplyChangement();
	  GetSetting().Pause();
	}
    }

    bool
    DialogSimulation::IsPossibleToClose()
    {
      if (Core::SystemSettingT<Setting::Simulation>::Get().IsRunning())
	{
	  OMC_INFO_MSG(tr("Please stop your simulation first"));
	  return false;
	}
      return true;
    }

    
    void 
    DialogSimulation::Close()
    {
      player_->Stop();
      
      

      if (Check())
	ApplyChangement();
      
      Ok();
      //Cancel();
      //Reset();
    }

    void 
    DialogSimulation::Ok()
    {
      DialogSystemSetting::Ok();
    }

  }
}
