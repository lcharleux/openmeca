// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_DialogPartUserJunction_hpp
#define OpenMeca_Gui_DialogPartUserJunction_hpp

#include "OpenMeca/Gui/Dialog/DialogUserItemT.hpp"
#include "OpenMeca/Core/Singleton.hpp"
#include "OpenMeca/Core/AutoRegisteredPtr.hpp"
#include "OpenMeca/Gui/Prop/PropString.hpp"
#include "OpenMeca/Gui/Prop/PropSelectItemT.hpp"



namespace OpenMeca
{
  namespace Item
  {
    class PartUserJunction;
    class PartPoint;
  }
}

namespace OpenMeca
{
  namespace Gui
  {
    class DialogPartUserJunction : public DialogUserItemT<Item::PartUserJunction>, 
				   public Core::Singleton<DialogPartUserJunction>
    {
      
    public:
      DialogPartUserJunction();
      ~DialogPartUserJunction();
      bool Check();

    private:
      void Init();

    private:
      PropString id_; 
      PropSelectItemT< Core::AutoRegisteredPtr<Item::PartPoint, Item::PartUserJunction > > endPoint_;
      
    };

  }
}
#endif
