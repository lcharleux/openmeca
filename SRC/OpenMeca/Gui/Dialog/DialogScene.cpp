// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <iostream>


#include "OpenMeca/Setting/Scene.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Gui/Dialog/DialogScene.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    DialogScene::DialogScene()
      :DialogUserItemT<Setting::Scene>(),
       drawAxis_(this),
       backgroundColor_(this),
       sceneCenter_(this),
       sceneRadius_(this),
       cameraType_(this)
    {
      drawAxis_.SetLabel(QObject::tr("Draw scene axis"));

      backgroundColor_.SetLabel(QObject::tr("Background color"));
      
      sceneCenter_.SetLabel(QObject::tr("Scene center"));
      sceneCenter_.SetDimension(Util::Dimension::Get("Length"));

      sceneRadius_.SetLabel(QObject::tr("Scene radius"));
      sceneRadius_.SetDimension(Util::Dimension::Get("Length"));

      cameraType_.Prop::SetLabel(QObject::tr("Camera"));

      GetPropTree().Add(drawAxis_);
      GetPropTree().Add(backgroundColor_);
      GetPropTree().Add(sceneCenter_);
      GetPropTree().Add(sceneRadius_);
      GetPropTree().Add(cameraType_);
    }

    DialogScene::~DialogScene()
    {
    }

    void
    DialogScene::Init()
    {
      drawAxis_.SetValue(GetCurrentItem().GetDrawAxis());
      backgroundColor_.SetValue(GetCurrentItem().GetBackgroundColor());
      sceneCenter_.SetValue(GetCurrentItem().GetSceneCenter());
      sceneRadius_.SetValue(GetCurrentItem().GetSceneRadius());
      cameraType_.SetValue(GetCurrentItem().GetCameraType());
    }

  

  }
}
