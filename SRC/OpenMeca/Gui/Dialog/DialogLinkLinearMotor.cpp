// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Gui/Dialog/DialogLinkLinearMotor.hpp"
#include "OpenMeca/Item/Link/LinearMotor.hpp"
#include "OpenMeca/Item/LinkT.hpp"


namespace OpenMeca
{
  namespace Gui
  {

  
    DialogLinkLinearMotor::DialogLinkLinearMotor()
      :DialogLinkT<Item::LinkT<Item::LinearMotor> >(),
       displacement_(this)
    {
      displacement_.SetLabel(QObject::tr("Displacement"));
      displacement_.SetDimension(Util::Dimension::Get("Length"));
      GetPropTree().Add(displacement_);
    }

    
    DialogLinkLinearMotor::~DialogLinkLinearMotor()
    {
    }

    void
    DialogLinkLinearMotor::Init()
    {
      DialogLinkT<Item::LinkT<Item::LinearMotor> >::Init();
      displacement_.SetValue(GetCurrentItem().GetLinkType().GetDisplacement());
    }


    bool
    DialogLinkLinearMotor::Check()
    {      
      return DialogLinkT<Item::LinkT<Item::LinearMotor> >::Check();
    }


  }
}
