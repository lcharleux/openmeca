// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_DialogLoadT_hpp
#define OpenMeca_Gui_DialogLoadT_hpp

#include "OpenMeca/Gui/Dialog/DialogUserItemT.hpp"

#include "OpenMeca/Core/Singleton.hpp"
#include "OpenMeca/Core/AutoRegisteredPtr.hpp"
#include "OpenMeca/Gui/Prop/PropString.hpp"
#include "OpenMeca/Gui/Prop/PropSelectItemT.hpp"


namespace OpenMeca
{
  namespace Item
  {
    template<class Parent, class Quantity, class How>
    class LoadT;
  }
}

namespace OpenMeca
{
  namespace Gui
  {

    template<class Parent, class Quantity, class How>
    class DialogLoadT : public DialogUserItemT<Item::LoadT<Parent, Quantity, How> >, 
			public Core::Singleton<DialogLoadT<Parent, Quantity, How> >
    {
      
    public:
      DialogLoadT();
      ~DialogLoadT();

    private:
      void Init();
      virtual void ApplyChangement();

    private:
      PropString id_;
      PropSelectItemT< Core::AutoRegisteredPtr<Parent, Item::LoadT<Parent, Quantity, How> > > parent_;
      typename Quantity::GuiManager phys_;

    };

    template<class Parent, class Quantity, class How>
    inline
    DialogLoadT<Parent, Quantity, How>::DialogLoadT()
      :DialogUserItemT<Item::LoadT<Parent, Quantity, How> >(),
       id_(this),
       parent_(this),
       phys_(this)
    {
      id_.SetLabel(QObject::tr("Name"));
      parent_.Prop::SetLabel(QObject::tr("Parent"));
      
      // Init table
      DialogUserItem::GetPropTree().Add(id_);
      DialogUserItem::GetPropTree().Add(parent_);
      phys_.Add(DialogUserItem::GetPropTree());
    }
  
    template<class Parent, class Quantity, class How>
    inline
    DialogLoadT<Parent, Quantity, How>::~DialogLoadT()
    {
    }

    template<class Parent, class Quantity, class How>
    inline void
    DialogLoadT<Parent, Quantity, How>::Init()
    {
      Item::LoadT<Parent, Quantity, How>& currentItem = 
	DialogUserItemT<Item::LoadT<Parent, Quantity, How> >::GetCurrentItem();

      id_.SetValue(currentItem.GetName());

      // populate parent list
      Core::SetOfBase<Parent>& parent = 
	Core::System::Get().GetSetOf<Parent>();
      OMC_ASSERT_MSG(parent.GetTotItemNumber() > 0, "There is not any item in the parent set");
      parent_.SetList(parent);
      parent_.SetValue(currentItem.GetParentItemPtr());

      phys_.Init(currentItem.GetQuantity());

    }

    template<class Parent, class Quantity, class How>
    inline void
    DialogLoadT<Parent, Quantity, How>::ApplyChangement()
    {
      DialogUserItemT<Item::LoadT<Parent, Quantity, How> >::ApplyChangement();
      DialogUserItemT<Item::LoadT<Parent, Quantity, How> >::GetCurrentItem().GetQuantity().Init();      
    }
    
  }
}
#endif
