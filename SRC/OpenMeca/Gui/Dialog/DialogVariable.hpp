// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_Dialog_DialogVariable_hpp
#define OpenMeca_Gui_Dialog_DialogVariable_hpp

#include "OpenMeca/Gui/Dialog/DialogUserItemT.hpp"
#include "OpenMeca/Core/Singleton.hpp"

#include "OpenMeca/Gui/Prop/PropDouble.hpp"
#include "OpenMeca/Gui/Prop/PropString.hpp"



namespace OpenMeca
{
  namespace Item
  {
    class Variable;
  }
}

namespace OpenMeca
{
  namespace Gui
  {
    class DialogVariable : public DialogUserItemT<Item::Variable>, 
			   public Core::Singleton<DialogVariable>
    {
      
    public:
      DialogVariable();
      ~DialogVariable();

    private:
      void Init();

    protected:
      virtual void ApplyChangement();
      virtual void CancelChangement();
      virtual bool Check();

    private:
      PropString symbol_;
      PropDouble value_;

    };

  }
}
#endif
