// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Physic/Force.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    namespace Physic
    {
    
      DialogForce::DialogForce(QWidget* widget)
	:DialogMechanicalAction(widget)
      {
	const std::string & unitStr = OpenMeca::Physic::Force::GetStrType();
	DialogMechanicalAction::x_.SetDimension(Util::Dimension::Get(unitStr));
	DialogMechanicalAction::y_.SetDimension(Util::Dimension::Get(unitStr));
	DialogMechanicalAction::z_.SetDimension(Util::Dimension::Get(unitStr));
      }

      DialogForce::~DialogForce()
      {
      }
      
      void 
      DialogForce::Add(Gui::PropTree& prop)
      {
	DialogMechanicalAction::Add(prop);
      }
      
      void 
      DialogForce::Init(OpenMeca::Physic::Force& f)
      {
	DialogMechanicalAction::Init(f);
      }
    
    }
  }
}
