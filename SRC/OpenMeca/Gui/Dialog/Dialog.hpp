// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_Dialog_hpp 
#define OpenMeca_Gui_Dialog_hpp 

#include <QWidget>
#include <QAction>

#include "OpenMeca/Core/SetOf.hpp"


namespace OpenMeca
{

  namespace Gui
  {
    
    class Prop;


    class Dialog : public QWidget
    {
      Q_OBJECT
      
     protected:
      Dialog();
      virtual ~Dialog();
      
    public:
      void AddProp(Prop&);
      void RemoveProp(Prop& w){widgets_.RemoveItem(w);}; // Must be here du to mingw32 link bug

      virtual bool Check();
      virtual void Ok() = 0;
      virtual void Cancel() = 0;
      virtual bool IsPossibleToClose(); 

    protected : 
      void Show_CallBack();
      void Hide_CallBack();
      virtual void ApplyChangement();
      virtual void CancelChangement();
      void SetAction(QAction& action);
      QAction& GetAction();
      virtual void Reset();
      void AddActionToHistoric();


    protected:
      Core::SetOfBase<Prop> widgets_;
      
    private:
      QAction* action_;

    };

  }
}

#endif
