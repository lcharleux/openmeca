// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/Shape/Sphere.hpp"
#include "OpenMeca/Gui/Dialog/Shape/DialogSphere.hpp"
#include "OpenMeca/Gui/Dialog/Shape/DialogSphere.hpp"
#include "OpenMeca/Gui/Prop/PropTree.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    namespace Shape
    {
    
      DialogSphere::DialogSphere(QWidget* widget):
	radius_(widget)
      {
	radius_.SetDimension(Util::Dimension::Get("Length"));
	radius_.SetLabel(QObject::tr("Radius"));
	radius_.AddCondition(new Core::Superior<double>(0.));
      }
      
      void 
      DialogSphere::Add(Gui::PropTree& prop)
      {
	prop.Add(radius_);
      }
      
      void 
      DialogSphere::Init(OpenMeca::Item::Shape::Sphere& s)
      {
	radius_.SetValue(s.GetRadius());
      }
    
    }
  }
}
