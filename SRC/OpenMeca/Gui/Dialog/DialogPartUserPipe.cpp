// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QVBoxLayout>

#include "OpenMeca/Gui/Dialog/DialogPartUserPipe.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Item/PartUserPipe.hpp"
#include "OpenMeca/Util/Dimension.hpp"

namespace OpenMeca
{
  namespace Gui
  {
  
    DialogPartUserPipe::DialogPartUserPipe()
      :DialogUserItemT<Item::PartUserPipe>(),
       id_(this),
       parent_(this),
       axis_(this),
       length_(this)
    {
      id_.SetLabel(QObject::tr("Name"));
      parent_.Prop::SetLabel(QObject::tr("Attached to point"));
      axis_.SetLabel(QObject::tr("Axis"));
      axis_.SetDimension(Util::Dimension::Get("Length"));

      length_.SetLabel(QObject::tr("Length"));      
      length_.SetDimension(Util::Dimension::Get("Length"));

      // Init table
      GetPropTree().Add(id_);
      GetPropTree().Add(parent_);
      GetPropTree().Add(axis_);
      GetPropTree().Add(length_);

    }
    
    DialogPartUserPipe::~DialogPartUserPipe()
    {
    }
    
    void
    DialogPartUserPipe::Init()
    {
      id_.SetValue(GetCurrentItem().GetName());
      axis_.SetValue(GetCurrentItem().GetAxis());
      length_.SetValue(GetCurrentItem().GetLength());

      // populate parent list
      Core::SetOfBase<OpenMeca::Item::PartPoint>& points = 
	Core::System::Get().GetSetOf<OpenMeca::Item::PartPoint>();
      
      Core::SetOfBase<Core::UserItem> set;
      for (unsigned int i = 0; i < points.GetTotItemNumber(); ++i)
	{
	  Core::UserItem& item = points(i);
	  if (!GetCurrentItem().HasChild(item))
	    set.AddItem(item);
	}
      OMC_ASSERT_MSG(set.GetTotItemNumber() > 0, "The set number is null");
      parent_.SetList(set);
      parent_.SetValue(GetCurrentItem().GetParentItemPtr());
      
    }
    
  }
}
