// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_DialogLink_hpp
#define OpenMeca_Gui_DialogLink_hpp

#include <QWidget>
#include "OpenMeca/Item/Link.hpp"
#include "OpenMeca/Core/AutoRegisteredPtr.hpp"
#include "OpenMeca/Gui/Prop/PropSelectItemT.hpp"
#include "OpenMeca/Gui/Prop/PropDouble.hpp"
#include "OpenMeca/Gui/Prop/PropExprPoint.hpp"
#include "OpenMeca/Gui/Prop/PropExprAttitude.hpp"
#include "OpenMeca/Gui/Prop/PropMotionLaw.hpp"

namespace OpenMeca
{
  namespace Gui
  {

    class DialogUserItem;
    
    class DialogLink
    {
      
    public:
      DialogLink(DialogUserItem* me);
      virtual ~DialogLink();

    protected:
      bool Check();
      PropSelectItemT<Core::AutoRegisteredPtr<Item::Body, Item::Link> >& GetPropSelectItemBody1();
      PropSelectItemT<Core::AutoRegisteredPtr<Item::Body, Item::Link> >& GetPropSelectItemBody2();
      virtual Item::Link& GetCurrentLink() = 0;
      void Init();

    private:
      DialogUserItem& me_;
      PropString id_; 
      PropSelectItemT<Core::AutoRegisteredPtr<Item::Body, Item::Link> > body1_;
      PropSelectItemT<Core::AutoRegisteredPtr<Item::Body, Item::Link> > body2_;
      PropExprPoint point_;
      PropExprAttitude quaternion_;
      PropBool drawLocalFrame_;

      
    };


   

  }
}
#endif
