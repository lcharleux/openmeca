// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef OpenMeca_Gui_Dialog_DialogUserItemT_hpp
#define OpenMeca_Gui_Dialog_DialogUserItemT_hpp

#include <iostream>
#include <QAction>

#include "OpenMeca/Gui/Dialog/DialogUserItem.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    template <class T>
    class DialogUserItemT : public DialogUserItem
   {

   public:
     void New(QAction& action);
     template<class Parent> void New(QAction& action, Parent& parent);
     void Edit(QAction& action, T& item);
      
   protected:
     DialogUserItemT();
     virtual ~ DialogUserItemT();
     Core::UserItem& GetCurrentUserItem();
     T& GetCurrentItem();
     bool IsCurrentUserItemStillExist();
     
   private:
     void Reset();

    private:
     T* currentItem_;
      
    };

    template <class T>
    inline
    DialogUserItemT<T>::DialogUserItemT()
      : DialogUserItem(),
       currentItem_(0)
    {
    }
    
    template <class T>
    inline
    DialogUserItemT<T>::~DialogUserItemT()
    {
    }
    
    template <class T>
    inline void
    DialogUserItemT<T>::New(QAction& action)
    {
      if (!MainWindow::Get().CloseDialog(true))
	return ;
      
      if (IsNewItemAllowed())
	{
	  Dialog::Show_CallBack(); 
	  DialogUserItem::ActiveNewItemMode();
	  DialogUserItem::SetAction(action);
	  OMC_ASSERT_MSG(currentItem_ == 0, "the current item is not null");
	  currentItem_ = new T();
	  typename T::Dialog* me = static_cast< typename T::Dialog*>(this);
	  QString title = QObject::tr("New ") + T::GetQStrType();
	  MainWindow::Get().GetDialogContainer().insertItem(1, me, title);
	  MainWindow::Get().GetDialogContainer().setCurrentIndex(1);
	  Init();
	  me->show();
	}
    }

    template <class T>
    template <class Parent>
    inline void
    DialogUserItemT<T>::New(QAction& action, Parent& parent)
    {
      if (!MainWindow::Get().CloseDialog(false))
	return ;

      if (IsNewItemAllowed())
	{
	  Dialog::Show_CallBack();
	  DialogUserItem::ActiveNewItemMode();
	  DialogUserItem::SetAction(action);
	  OMC_ASSERT_MSG(currentItem_ == 0, "the current item is not null");
	  currentItem_ = new T(parent);
	  typename T::Dialog* me = static_cast< typename T::Dialog*>(this);
	  QString title = QString("New ") + QString(T::GetStrType().c_str());
	  MainWindow::Get().GetDialogContainer().insertItem(1, me, title);
	  MainWindow::Get().GetDialogContainer().setCurrentIndex(1);
	  Init();
	  me->show();
	}
    }


    template <class T>
    inline void
    DialogUserItemT<T>::Edit(QAction& action, T& item)
    {
      if (!MainWindow::Get().CloseDialog(true))
	return ;
      
      Dialog::Show_CallBack();
      DialogUserItem::DisableNewItemMode();
      DialogUserItem::SetAction(action);
      OMC_ASSERT_MSG(currentItem_ == 0, "the current item is not null");
      currentItem_ = &item;
      typename T::Dialog* me = static_cast< typename T::Dialog*>(this);
      QString title = QString("Edit ") + QString(T::GetStrType().c_str());
      MainWindow::Get().GetDialogContainer().insertItem(1, me, title);
      MainWindow::Get().GetDialogContainer().setCurrentIndex(1);
      Init();
      me->show();
    }

    template <class T>
    inline bool
    DialogUserItemT<T>::IsCurrentUserItemStillExist()
    {
      OMC_ASSERT_MSG(currentItem_ != 0, "the current item is null");
      return Core::AutoRegister<T>::Exist(*currentItem_);
    }
    
    template <class T>
    inline Core::UserItem& 
    DialogUserItemT<T>::GetCurrentUserItem()
    {
      OMC_ASSERT_MSG(currentItem_ != 0, "the current item is null");
      return reinterpret_cast<Core::UserItem& >(*currentItem_);
    }

    template <class T>
    inline T& 
    DialogUserItemT<T>::GetCurrentItem()
    {
      OMC_ASSERT_MSG(currentItem_ != 0, "the current item is null");
      return *currentItem_;
    }

    template <class T>
    inline void
    DialogUserItemT<T>::Reset()
    {
      DialogUserItem::Reset();
      currentItem_=0;
    }

  }
}

#endif
