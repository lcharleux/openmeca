// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_DialogLinkT_hpp
#define OpenMeca_Gui_DialogLinkT_hpp

#include <QMessageBox>
#include "OpenMeca/Gui/Dialog/DialogUserItemT.hpp"
#include "OpenMeca/Gui/Dialog/DialogLink.hpp"
#include "OpenMeca/Core/Singleton.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"


namespace OpenMeca
{
  namespace Gui
  {

    template <class Link>
    class DialogLinkT :  public DialogUserItemT<Link>, 
			 public Core::Singleton<DialogLinkT <Link> >,
			 public DialogLink
    {
      
    public:
      DialogLinkT();
      virtual ~DialogLinkT();
      virtual bool Check();
      virtual bool IsNewItemAllowed() const;

    protected:
      virtual void Init();
      virtual void ApplyChangement();
      Item::Link& GetCurrentLink();

    private:
      

    };


    template <class Link>
    inline 
    DialogLinkT <Link>::DialogLinkT()
      : DialogUserItemT<Link>(),
	DialogLink(this)
    {
    }
    
    template <class Link>
    inline
    DialogLinkT <Link>::~DialogLinkT()
    {
    }
    
    template <class Link>
    inline void
    DialogLinkT <Link>::Init()
    {
      DialogLink::Init();
    }

    template <class Link>
    inline Item::Link&
    DialogLinkT <Link>::GetCurrentLink()
    {
      return DialogUserItemT<Link>::GetCurrentItem(); 
    }



    template <class Link>
    inline bool
    DialogLinkT<Link>::Check()
    {
      if (!DialogLink::Check())
	return false;

      DialogUserItemT<Link>::GetCurrentItem().GetBody1().GetDependentItems().UnCheckForDoublet();
      DialogUserItemT<Link>::GetCurrentItem().GetBody2().GetDependentItems().UnCheckForDoublet();      
      bool ok = DialogUserItem::Check(); 

      DialogUserItemT<Link>::GetCurrentItem().GetBody1().GetDependentItems().CheckForDoublet();
      DialogUserItemT<Link>::GetCurrentItem().GetBody2().GetDependentItems().CheckForDoublet();   

      return ok;
    }

    template <class Link>
    inline void
    DialogLinkT<Link>::ApplyChangement()
    {
      DialogUserItemT<Link>::GetCurrentItem().GetBody1().GetDependentItems().UnCheckForDoublet();
      DialogUserItemT<Link>::GetCurrentItem().GetBody2().GetDependentItems().UnCheckForDoublet(); 
      DialogUserItem::ApplyChangement();
      DialogUserItemT<Link>::GetCurrentItem().GetBody1().GetDependentItems().CheckForDoublet();
      DialogUserItemT<Link>::GetCurrentItem().GetBody2().GetDependentItems().CheckForDoublet();  
    }


    template <class Link>
    inline bool
    DialogLinkT<Link>::IsNewItemAllowed() const
    {
      if (Core::System::Get().GetSetOf<Item::Body>().GetTotItemNumber() < 2)
	{
	  const QString msg = QObject::tr("You must build 2 bodys before adding a new link");
	  const QWidget* me_const = this;
	  QWidget* me = const_cast<QWidget*>(me_const);
	  QMessageBox::warning(me, QObject::tr("Warning"), msg);
	  return false;
	}
      return true;
    }
   
   


  }
}
#endif
