 // This file is part of OpenMeca, an easy software to do mechanical simulation.
//
 // Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QVBoxLayout>

#include "OpenMeca/Gui/Dialog/DialogPartUserJunction.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Item/PartUserJunction.hpp"
#include "OpenMeca/Item/PartPoint.hpp"
#include "OpenMeca/Item/Body.hpp"

namespace OpenMeca
{
  namespace Gui
  {
  
    DialogPartUserJunction::DialogPartUserJunction()
      :DialogUserItemT<Item::PartUserJunction>(),
       id_(this),
       endPoint_(this)
    {
      id_.SetLabel(QObject::tr("Name"));
      endPoint_.Prop::SetLabel(QObject::tr("End Point"));

            // Init table
      GetPropTree().Add(id_);
      GetPropTree().Add(endPoint_);
    }
    
    DialogPartUserJunction::~DialogPartUserJunction()
    {
    }
    
    void
    DialogPartUserJunction::Init()
    {
      Core::SetOfBase<Item::PartPoint> set = GetCurrentItem().GetBody().GetChildSet<Item::PartPoint>();
      // Check that the number of available point
      if (set.GetTotItemNumber() < 2)
	{
	  QMessageBox::warning(this, QObject::tr("Warning"), 
			       QObject::tr("Need two points to build a junction"));
	  hide();
	  return;
	}

      endPoint_.SetList(set);

      id_.SetValue(GetCurrentItem().GetName());
      endPoint_.SetValue(GetCurrentItem().GetEndPointPtr());
      
    }
    
    
    bool
    DialogPartUserJunction::Check()
    {
      if (&endPoint_.GetSelectedItem() == &GetCurrentItem().GetStartPoint())
	{
	  QMessageBox::warning(this, QObject::tr("Warning"), 
			       QObject::tr("The end point is the same as the start point"));
	  return false;
	}
      return true;
    }

    
    
  }
}
