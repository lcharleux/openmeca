// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_DialogPartUserPipe_hpp
#define OpenMeca_Gui_DialogPartUserPipe_hpp


#include "OpenMeca/Core/AutoRegisteredPtr.hpp"
#include "OpenMeca/Core/Singleton.hpp"

#include "OpenMeca/Gui/Dialog/DialogUserItemT.hpp"
#include "OpenMeca/Gui/Prop/PropAttitude.hpp"
#include "OpenMeca/Gui/Prop/PropString.hpp"
#include "OpenMeca/Gui/Prop/PropDouble.hpp"
#include "OpenMeca/Gui/Prop/PropVector.hpp"
#include "OpenMeca/Gui/Prop/PropSelectItemT.hpp"

#include "OpenMeca/Item/PartUser.hpp"

namespace OpenMeca
{
  namespace Item
  {
    class PartUserPipe;
  }
}

namespace OpenMeca
{
  namespace Gui
  {
    class DialogPartUserPipe : public DialogUserItemT<Item::PartUserPipe>, 
			       public Core::Singleton<DialogPartUserPipe>
    {
      
    public:
      DialogPartUserPipe();
      ~DialogPartUserPipe();

    private:
      void Init();

    private:
      PropString id_; 
      PropSelectItemT< Core::AutoRegisteredPtr<Core::UserItem, Item::PartUser > > parent_;
      PropVector axis_;
      PropDouble length_;
      
    };

  }
}
#endif
