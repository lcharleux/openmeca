// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Gui_Viewer_hpp
#define OpenMeca_Gui_Viewer_hpp

#include <QMessageBox>
#include "QGLViewer/qglviewer.h"
#include "OpenMeca/Geom/Point.hpp"

namespace OpenMeca
{
  namespace Gui
  {
    class Viewer : public QGLViewer
    {
      Q_OBJECT
      
    public:
      Viewer(QWidget * parent);
      ~Viewer();
      void SetSnapShotMode(bool);
      void DrawAxis(bool);
      void DrawText(const std::string&, const Geom::Point<_3D>&);
      void DrawCornerAxis();


    protected :
      virtual void draw(); 
      virtual void drawWithNames();
      virtual void postDraw();
      virtual void postSelection(const QPoint& point);
      virtual void init();
      virtual void animate();
      virtual void mousePressEvent(QMouseEvent*);

    private:
      bool rightClick_;
      bool snapshot_;
      bool drawAxis_;

      
    };

  }
}
#endif
