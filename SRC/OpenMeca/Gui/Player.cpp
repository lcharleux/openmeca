// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QImageReader>
#include <QFileDialog>

#include "OpenMeca/Gui/Player.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Gui/ImageDialog.hpp"

namespace OpenMeca
{
  namespace Gui
  {
  

    Player::Player(QWidget * parent)
      :QWidget(parent),
       timeLabelPrefix_(tr("Simulated time : ")),
       iterLabelPrefix_(tr("Iteration number : ")),
       step_()
    {
      
      setupUi(this);
      Setting::Simulation* simu = &Core::SystemSettingT<Setting::Simulation>::Get();
      qRegisterMetaType<OpenMeca::Setting::Simulation::Step>("OpenMeca::Setting::Simulation::Step");

      QObject::connect(compute_, SIGNAL(toggled(bool)),    this, SLOT(TogglePlay(bool)));
      QObject::connect(stop_,    SIGNAL(clicked()),        this, SLOT(Stop()));
      QObject::connect(slider_,  SIGNAL(sliderMoved(int)), this, SLOT(Move(int)));
      QObject::connect(simu,     SIGNAL(End()),            this, SLOT(End()));
      QObject::connect(record_,  SIGNAL(clicked())       , this, SLOT(Record()));

      QObject::connect(simu, SIGNAL(StepChanged(OpenMeca::Setting::Simulation::Step)), 
		       this, SLOT(Update(OpenMeca::Setting::Simulation::Step)));


      QObject::connect(this, SIGNAL(MovedTo(unsigned int)), 
		       simu, SLOT(GoToStep(unsigned int)));
      step_.Init();
      slider_->setMinimum(0);
      UpdateWidget();
    }
    
    Player::~Player()
    {
    }

    void
    Player::TogglePlay(bool play)
    {
      if (play)
	Play();
      else
	Pause();
    }
    
    void
    Player::Stop()
    {
      // compute_->setChecked(false);
      // Pause();
      emit Stopped();
      Core::SystemSettingT<Setting::Simulation>::Get().wait();
      compute_->setChecked(false);
    }

    void
    Player::Play()
    {
      OMC_ASSERT_MSG(compute_->isChecked() == true, "The compute button must be checked");
      emit Played();
    }

    void
    Player::Pause()
    {
      OMC_ASSERT_MSG(compute_->isChecked() == false, "The compute button must not be checked");
      emit Paused();
      Core::SystemSettingT<Setting::Simulation>::Get().wait();
    }

    void
    Player::End()
    {
      compute_->setChecked(false);
      Pause();
    }


  
    void
    Player::Update(OpenMeca::Setting::Simulation::Step step)
    {
      step_ = step;
      UpdateWidget();
    }

    void
    Player::UpdateWidget()
    {
      time_->setText(timeLabelPrefix_ + QString::number(step_.time) + " s");
      iter_->setText(iterLabelPrefix_ + QString::number(step_.iterNumber) + " / " + QString::number(step_.totalIterNumber));
      slider_->setMaximum(step_.totalIterNumber);
      slider_->setSliderPosition(step_.iterNumber);    
    }
    
    void
    Player::Move(int pos)
    {
      compute_->setChecked(false);
      const unsigned int iterNumber = pos; 
      emit MovedTo(iterNumber);
    }

    
    QStringList
    Player::GetSupportedImageFormat() const
    {
      QList<QByteArray> formatList = QImageReader::supportedImageFormats();
      QStringList strList;
      for (int i = 0; i<formatList.size();++i)
	strList.push_back(QString(formatList[i]));
      
      return strList;
    }


    void
    Player::Record()
    {
      Pause();
      Viewer& viewer = MainWindow::Get().GetViewer();
      bool toggle = record_->isChecked();
      if (toggle)
	{
	  int quality = 0;
	  QString format = "";
	  QStringList strList = GetSupportedImageFormat();
	  ImageDialog dialog(this, GetSupportedImageFormat(), quality, format);
	  if (dialog.exec())
	    {
	      viewer.setSnapshotFormat(format);
	      viewer.setSnapshotQuality(quality);
	      QString dir = QFileDialog::getExistingDirectory(this, tr("Save files in directory"),"", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	      if (dir!="")
		{
		  viewer.SetSnapShotMode(true);
		  viewer.setSnapshotFileName(dir+QDir::separator()+"save."+format);
		  return;
		}
	    }
	}
      record_->setChecked(false);
      viewer.SetSnapShotMode(false);
    }
    
  }
}
