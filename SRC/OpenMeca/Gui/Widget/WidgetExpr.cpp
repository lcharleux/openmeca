// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QToolTip>
#include <iostream>

#include "OpenMeca/Gui/Widget/WidgetExpr.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Util/Unit.hpp"
#include "OpenMeca/Core/Macro.hpp"

namespace OpenMeca
{
  namespace Gui
  {

    WidgetExpr::WidgetExpr(QWidget* parent)
      :QWidget(parent),
       dim_(0)
    {
      Ui::WidgetExpr::setupUi(this);
    }

    WidgetExpr::~WidgetExpr()
    {
      for (unsigned int i = 0; i < condition_.size(); ++i)
	delete condition_[i];
    }

    
    void 
    WidgetExpr::SetDimension(const Util::Dimension& dim)
    {
      dim_ = &dim;
      unitLabel_->setText(dim_->GetUserChoice().GetSymbol().c_str());
    }

    const Util::Dimension&
    WidgetExpr::GetDimension() const
    {
      OMC_ASSERT_MSG(dim_!=0, "The widget double has no dimension");
      return *dim_;
    }

    void 
    WidgetExpr::SetExpr(const Util::Expr& expr)
    {
      OMC_ASSERT_MSG(dim_ == &expr.GetDimension(),
		     "The specified dimension are not equals");

      const QString str =  expr.ToString().c_str();
      GetLineEdit().setText(str);
      ComputeExpression();
    }
   
    bool
    WidgetExpr::ComputeExpression()
    {
      const std::string exp = GetLineEdit().text().toStdString();

      if (Util::Expr::IsValid(exp) == false)
	{
	  DisplayHelp("The expression is not valid");
	  result_->setText("=#@? ");
	  return false;
	}
      result_->setText("=" + QString::number(Util::Expr::Compute(exp)) + " ");
      return true;
    }
    

    bool
    WidgetExpr::GetExpr(Util::Expr& val)
    {
      const bool expr_ok = ComputeExpression();
      if (expr_ok)
	val.SetString(GetLineEdit().text().toStdString());
      return expr_ok;
    }

    QLineEdit&
    WidgetExpr::GetLineEdit()
    {
      return *lineEdit_;
    }

    void 
    WidgetExpr::DisplayHelp(const QString& msg)
    {
      QToolTip::showText(mapToGlobal( QPoint( 0, 0 ) ), msg);
    }

    void 
    WidgetExpr::AddCondition(const Core::Condition<double>* cond)
    {
      condition_.push_back(cond);
    }

    bool
    WidgetExpr::CheckCondition(const double& val)
    {
      for (unsigned int i = 0; i < condition_.size(); ++i)
	{
	  if (condition_[i]->Check(val) == false)
	    {
	      DisplayHelp(condition_[i]->ErrorMessage());
	      return false;
	    }
	}
      return true;
    }


  }
}

