// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QApplication>
#include <iostream>

#include "OpenMeca/Gui/Widget/WidgetScales.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/System.hpp"


namespace OpenMeca
{
  namespace Gui
  {
    WidgetScales::WidgetScales(QWidget* parent)
      :QWidget(parent),
       editScale_(new QAction(QString("Edit Scale"), this)),
       index_()
    {
      Ui::WidgetScales::setupUi(this);

      QObject::connect(doubleSpinBox_, SIGNAL(valueChanged(double)), 
		       this, SLOT(ValueChanged(double)));
      
      QObject::connect(label_, SIGNAL(currentIndexChanged(const QString&)), 
		       this, SLOT(ScaleChanged(const QString&)));
      

      OpenMeca::Setting::Scales& scales = Core::System::Get().GetScales();
      scales.SetWidget(*this);

      std::vector<std::string> vec = scales.GetKeys();
      for (unsigned int i=0; i < vec.size(); i++)
	{
	  index_[i] = vec[i];
	  label_->addItem(QObject::tr(vec[i].c_str()));
	}
      Update();
    }

    WidgetScales::~WidgetScales()
    {
    }

    double& 
    WidgetScales::GetScaleValue()
    {
      const int i = label_->currentIndex();
      OMC_ASSERT_MSG(index_.count(i)==1, "The scale has no index");
      return Core::System::Get().GetScales().GetScaleValue(index_[i]);
    }
    
    void 
    WidgetScales::UpdateStep()
    {
      doubleSpinBox_->setSingleStep(GetScaleValue()*100./2.);
    }

    void 
    WidgetScales::Update()
    {
      QObject::disconnect(doubleSpinBox_, SIGNAL(valueChanged(double)), this, SLOT(ValueChanged(double)));
      doubleSpinBox_->setValue(GetScaleValue()*100.);
      UpdateStep();
      QObject::connect(doubleSpinBox_, SIGNAL(valueChanged(double)), this, SLOT(ValueChanged(double)));
    }

    void 
    WidgetScales::ScaleChanged(const QString&)
    {
      Update();
    }


    void
    WidgetScales::ValueChanged(double currentValue)
    {
      GetScaleValue() = currentValue/100.;
      UpdateStep();
      IsEdited();
    }
   
    void
    WidgetScales::IsEdited()
    {
      Core::System::Get().GetHistoric().SystemEdited();
      MainWindow::Get().AddToHistoric(*editScale_);
      MainWindow::Get().GetViewer().updateGL(); 
    }


  }
}

