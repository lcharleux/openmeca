// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <cstdlib>

#include <QApplication>
#include <QSplashScreen>
#include <QBitmap>
#include <QThread>

#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/Software.hpp"
#include "OpenMeca/Setting/LangManager.hpp"
#include "ChronoEngine/physics/ChApidll.h"



int main(int argc, char *argv[])
{
  chrono::DLL_CreateGlobals();
  
  

  QApplication app(argc, argv);
  OpenMeca::Core::Singleton<OpenMeca::Setting::LangManager>::Get().ReadXmlFile();
  OpenMeca::Core::Singleton<OpenMeca::Setting::LangManager>::Get().Apply(app);

  QString welcome_msg = QObject::tr("Welcome to openmeca") + " version";
  welcome_msg += OpenMeca::Core::Software::Get().Version().ToString().c_str();
  OpenMeca::Core::DisplayInfoMsg(welcome_msg);

  std::string fileName = "";
  if (argc == 2) 
    fileName = argv[1];

  OpenMeca::Gui::MainWindow*  window = new OpenMeca::Gui::MainWindow(fileName);
  window->show();
  int res = app.exec();

  chrono::DLL_DeleteGlobals();
  delete window;

  OpenMeca::Core::DisplayInfoMsg(QObject::tr("Good bye, see you soon !"));
  return res;
} 
