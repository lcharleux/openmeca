// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.



#include "OpenMeca/Item/Sensor.hpp"
#include "OpenMeca/Setting/Simulation.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Gui/RootTreeItemT.hpp"

#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(OpenMeca::Item::Sensor)


namespace OpenMeca
{
  namespace Item
  {    

    const std::string 
    Sensor::GetStrType()
    {
      return std::string("Sensor");
    }

    const QString 
    Sensor::GetQStrType()
    {
      return QObject::tr("Sensor");
    }

    void 
    Sensor::AcquireAll()
    {
      Core::SetOfBase<Sensor>& sensor = Core::System::Get().GetSetOf<Sensor>();
      for (unsigned int i = 0; i < sensor.GetTotItemNumber(); ++i)
	sensor(i).Acquire();
	  
    }

       
    Sensor::Sensor(const std::string strType, QTreeWidgetItem& parentTreeItem)
      :Physical(strType, parentTreeItem),
       treeItem_(0)
    {
      QTreeWidgetItem& rootItem = Gui::RootTreeItemT<Sensor>::Get();
      Core::Item& me = *this;
      treeItem_ = new Gui::SecondaryTreeItem(rootItem, me);
      AddSecondaryTreeItem(*treeItem_);
    }
    
    Sensor::~Sensor()
    {      
      delete treeItem_;
    }

    bool 
    Sensor::IsLoad() const
    {
      return false;
    }
    
    bool 
    Sensor::IsSensor() const
    {
      return true;
    }

  } 
}
