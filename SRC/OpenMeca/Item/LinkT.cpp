// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/UserRootItemCommonProperty.hpp"

#include "OpenMeca/Item/Link/All.hpp"

#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::Cylindrical>)
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::Gear>)
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::Pulley>)
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::RackPinion>)
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::Spring>)
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::Motor>)
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::LinearMotor>)
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::Planar>)
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::PointLine>)
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::PointPlane>)
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::Revolute>)
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::Screw>)
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::Slider>)
BOOST_CLASS_EXPORT(OpenMeca::Item::LinkT<OpenMeca::Item::Spherical>)


namespace OpenMeca
{  
  namespace Item
  {
    
    template<>
    void
    LinkT<Cylindrical>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildDefaultChSystem(chSystem);
    }

    template<>
    void
    LinkT<Planar>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildDefaultChSystem(chSystem);
    }

    template<>
    void
    LinkT<PointLine>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildDefaultChSystem(chSystem);
    }

    template<>
    void
    LinkT<PointPlane>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildDefaultChSystem(chSystem);
    }

    template<>
    void
    LinkT<Revolute>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildDefaultChSystem(chSystem);
    }

    template<>
    void
    LinkT<Slider>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildDefaultChSystem(chSystem);
    }

    template<>
    void
    LinkT<Spherical>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildDefaultChSystem(chSystem);
    }



    template<>
    void
    LinkT<Motor>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildChSystemInit();
      chrono::ChSharedBodyPtr& b1Ptr = GetBody1().GetChBodyPtr();
      chrono::ChSharedBodyPtr& b2Ptr = GetBody2().GetChBodyPtr();
      chLinkPtr_->Initialize(b1Ptr, b2Ptr, GetFrame().ToChCoordsys_Global());
      chLinkPtr_->Set_eng_mode(chrono::ChLinkEngine::ENG_MODE_SPEED);
      chLinkPtr_->Set_spe_funct(linkType_.GetVelocity().new_Duplicate());
      chSystem.AddLink(chLinkPtr_);
    }

    template<>
    void
    LinkT<LinearMotor>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildChSystemInit();
      chrono::ChSharedBodyPtr& b1Ptr = GetBody1().GetChBodyPtr();
      chrono::ChSharedBodyPtr& b2Ptr = GetBody2().GetChBodyPtr();
      chLinkPtr_->Initialize(b1Ptr, b2Ptr, GetFrame().ToChCoordsys_Global());
      chLinkPtr_->SetMotion_Z (linkType_.GetDisplacement().new_Duplicate());
      chSystem.AddLink(chLinkPtr_);
    }

    template<>
    void
    LinkT<Screw>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildChSystemInit();
      chrono::ChSharedBodyPtr& b1Ptr = GetBody1().GetChBodyPtr();
      chrono::ChSharedBodyPtr& b2Ptr = GetBody2().GetChBodyPtr();
      chLinkPtr_->Initialize(b1Ptr, b2Ptr, GetFrame().ToChCoordsys_Global());
      chLinkPtr_->Set_tau(linkType_.GetTau());
      chSystem.AddLink(chLinkPtr_);
    }

    template<>
    void
    LinkT<Gear>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildChSystemInit();
      chrono::ChSharedBodyPtr& b1Ptr = GetBody1().GetChBodyPtr();
      chrono::ChSharedBodyPtr& b2Ptr = GetBody2().GetChBodyPtr();
      chLinkPtr_->Initialize(b1Ptr, b2Ptr, chrono::CSYSNORM);
      chLinkPtr_->Set_local_shaft1(GetLinkType().ComputeLocalShaft<1>());
      chLinkPtr_->Set_local_shaft2(GetLinkType().ComputeLocalShaft<2>());

      chLinkPtr_->Set_alpha(linkType_.GetAngleOfAction());
      chLinkPtr_->Set_beta(0.);
      chLinkPtr_->Set_phase(0.);
      chLinkPtr_->Set_tau(linkType_.GetRatio());
      chLinkPtr_->Set_checkphase(false);
      
      if (linkType_.GetInternalTeeth())
	chLinkPtr_->Set_epicyclic(1);
      else
	chLinkPtr_->Set_epicyclic(0);

      chSystem.AddLink(chLinkPtr_);
    }


    template<>
    void
    LinkT<Pulley>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildChSystemInit();
      chrono::ChSharedBodyPtr& b1Ptr = GetBody1().GetChBodyPtr();
      chrono::ChSharedBodyPtr& b2Ptr = GetBody2().GetChBodyPtr();
      chLinkPtr_->Initialize(b1Ptr, b2Ptr, chrono::CSYSNORM);
      chLinkPtr_->Set_local_shaft1(GetLinkType().ComputeLocalShaft<1>());
      chLinkPtr_->Set_local_shaft2(GetLinkType().ComputeLocalShaft<2>());

      chLinkPtr_->Set_r1(linkType_.GetRadius1());
      chLinkPtr_->Set_r2(linkType_.GetRadius2());
      chLinkPtr_->Set_phase(0.);
      chLinkPtr_->Set_checkphase(false);
      
      chSystem.AddLink(chLinkPtr_);
    }


    template<>
    void
    LinkT<RackPinion>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildChSystemInit();
      chrono::ChSharedBodyPtr& b1Ptr = GetBody1().GetChBodyPtr();
      chrono::ChSharedBodyPtr& b2Ptr = GetBody2().GetChBodyPtr();
      chrono::ChFrame<> f1(GetBody1().GetFrame().ToChCoordsys());
      chrono::ChFrame<> f2(GetBody2().GetFrame().ToChCoordsys());

      chLinkPtr_->Initialize(b1Ptr, b2Ptr, true, f1, f2);
      chLinkPtr_->SetPinionFrame(GetLinkType().ComputeChFrame<1>());
      chLinkPtr_->SetRackFrame(GetLinkType().ComputeChFrame<2>());
      chLinkPtr_->SetPinionRadius(GetLinkType().GetPinionRadius());
      chLinkPtr_->SetAlpha(linkType_.GetAngleOfAction());
      chLinkPtr_->SetBeta(0.);
      chLinkPtr_->SetPhase(0.);
      chLinkPtr_->SetCheckphase(false);
      
      chSystem.AddLink(chLinkPtr_);
    }

    template<>
    void
    LinkT<Spring>::BuildChSystem(chrono::ChSystem& chSystem)
    {
      BuildChSystemInit();
      chrono::ChSharedBodyPtr& b1Ptr = GetBody1().GetChBodyPtr();
      chrono::ChSharedBodyPtr& b2Ptr = GetBody2().GetChBodyPtr();
      
      const Geom::Point<_3D> p1 = GetLinkType().GetStartPoint<1>();
      const Geom::Point<_3D> p2 = GetLinkType().GetStartPoint<2>();

      chLinkPtr_->Initialize(b1Ptr, b2Ptr, false, p1.ToChVector(), p2.ToChVector());
      chLinkPtr_->Set_SpringK (GetLinkType().GetStiffness());
      chLinkPtr_->Set_SpringRestLenght (GetLinkType().GetFreeLength());
      chLinkPtr_->Set_SpringR (GetLinkType().GetDampingFactor());
      chLinkPtr_->Set_SpringF (0);
      chSystem.AddLink(chLinkPtr_);
    }

  


  }

}

