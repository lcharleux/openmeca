// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


// This source file was inspired from the "libUtil" from 
// the GranOO workbench : http://www.granoo.org and

#ifndef _OpenMeca_Item_Physical_hpp_
#define _OpenMeca_Item_Physical_hpp_

#include <string>
#include <QMenu>
#include <QTreeWidget>
#include "OpenMeca/Core/DrawableUserItem.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/AutoRegisteredPtr.hpp"
#include "OpenMeca/Util/Unit.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Physic/Quantity.hpp"

namespace OpenMeca
{
  namespace Item
  {
    class Physical : public Core::DrawableUserItem, 
		     public Core::AutoRegister<Physical>
    {

    public:
      static const std::string GetStrType(); 
      static void DumpAllDataToFile(const std::string&);
   
    public:
      Physical(const std::string, QTreeWidgetItem&);
      virtual ~Physical();
      
      void FillDataTree(QTreeWidgetItem*);
      void UpdateDataTree();
      
      
      const Util::Unit& GetUnit() const; 
      void WriteHeaderDataFile(std::ofstream&) ;
      void WriteDataFile(std::ofstream&, unsigned int);

      virtual Physic::Quantity& GetQuantity() = 0;
      virtual const Physic::Quantity& GetQuantity() const = 0;

      virtual const UserItem& GetParent() const = 0;
      virtual UserItem& GetParent() = 0;

      virtual const Util::Color& GetColor() const;
      virtual Util::Color& GetColor();

      virtual const Body& GetBody() const;
      virtual Body& GetBody();

      virtual bool IsLoad() const = 0;
      virtual bool IsSensor() const = 0;

    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);



    };
    

    template<class Archive>
    inline void
    Physical::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Core::DrawableUserItem);
    }

  }
} 

#endif
