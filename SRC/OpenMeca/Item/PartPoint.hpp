// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef OpenMeca_Item_PartPoint_hpp
#define OpenMeca_Item_PartPoint_hpp


// This class is used for automatic point that don't support user edit

#include "OpenMeca/Item/PartUser.hpp"
#include "OpenMeca/Core/AutoRegisteredPtr.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/None.hpp"



namespace OpenMeca
{  

  namespace Item
  {

    // A part that represent a point. This is a little 3D sphere.
    class PartPoint: public PartUser, public Core::AutoRegister<PartPoint> 
    {    
    public:
      static const std::string GetStrType() {return std::string("AutomaticPoint");};
      static const QString GetQStrType() {return QObject::tr("AutomaticPoint");}; 

      static void Init();
      static void DrawIcon(QIcon&, QColor);
      typedef Core::None Dialog;


    public:
      PartPoint(Core::UserItem& parent);
      PartPoint(const std::string& childStrType, Core::UserItem& parent);
      virtual ~PartPoint();
      void DrawShape();
      void UpdateIcon();
      virtual void BeginDraw();

      void SetScaleCoordinate(double x, double y, double z);
      void SetAbsoluteCoordinate(double x, double y, double z);

    private:
      PartPoint(); //Not allowed, just for serialization
      PartPoint(const PartPoint&);             //Not Allowed
      PartPoint& operator=(const PartPoint&);  //Not Allowed


      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive& ar, const unsigned int);


    private:
      bool mustBeScaled_;
      Geom::Vector<_3D> scaleCoordinate_;
    };

    
    template<class Archive>
    inline void 
    PartPoint::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(PartUser);
      ar & BOOST_SERIALIZATION_NVP(mustBeScaled_);
      ar & BOOST_SERIALIZATION_NVP(scaleCoordinate_);
    }
    
    

    
  }

}


namespace boost 
{ 
  namespace serialization 
  {
    template<class Archive>
    inline void save_construct_data(Archive & ar, 
				    const OpenMeca::Item::PartPoint * t, 
				    const unsigned int)
    {
      const OpenMeca::Core::UserItem* parent = &t->GetParentItem();
      ar << parent;
    }
    
    template<class Archive>
    inline void load_construct_data(Archive & ar, 
				    OpenMeca::Item::PartPoint * t, 
				    const unsigned int)
    {
      OpenMeca::Core::UserItem* parent = 0;
      ar >> parent;
      ::new(t)OpenMeca::Item::PartPoint(*parent);
    }
  }
} // namespace ...

#endif
