// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Link_RackPinion_hpp
#define OpenMeca_Item_Link_RackPinion_hpp

#include <string>
#include <QIcon>
#include <QColor>

#include "OpenMeca/Item/PartPoint.hpp"
#include "OpenMeca/Item/Link/LinkTypeBase.hpp"
#include "OpenMeca/Gui/Dialog/DialogLinkRackPinion.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Item/Link_CreateAction_SpecializedT.hpp"

#include "ChronoEngine/physics/ChLinkRackpinion.h"
#include "ChronoEngine/physics/ChLinkLock.h"

namespace OpenMeca
{  
  namespace Item
  {

    class RackPinion : public LinkTypeBase
    {

    public:
      static const std::string GetStrType();
      static const QString GetQStrType();

      typedef chrono::ChLinkRackpinion ChLink; 
      typedef Gui::DialogLinkRackPinion Dialog;


    public:
      RackPinion(Link&);
      ~RackPinion();

      void UpdatePart();

      template <int N> void DrawPart();
      template <int N> void BuildPoints(Core::SetOf<PartPoint>&, Core::DrawableUserItem&);
      template <int N> double ComputeRadius();
      template <int N> chrono::ChFrame<> ComputeChFrame();

      // Accessors
      OMC_ACCESSOR(Ratio          , double ,  ratio_          );
      OMC_ACCESSOR(PinionRadius   , double ,  pinionRadius_);
      OMC_ACCESSOR(Modulus        , double ,  modulus_);
      OMC_ACCESSOR(AngleOfAction  , double ,  angleOfAction_);


    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    private:
      double ratio_;
      double pinionRadius_;
      double modulus_;
      double angleOfAction_;
    };

    template<class Archive>
    inline void
    RackPinion::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(LinkTypeBase);
      ar & BOOST_SERIALIZATION_NVP(ratio_);
      ar & BOOST_SERIALIZATION_NVP(pinionRadius_);
      ar & BOOST_SERIALIZATION_NVP(modulus_);
      ar & BOOST_SERIALIZATION_NVP(angleOfAction_);
    }


   

    template<int N>
    chrono::ChFrame<>
    RackPinion::ComputeChFrame()
    {
      const Geom::Point<_3D> p(GetLink().GetPart<N>().GetCenter(), 
			       GetLink().GetBody<N>().GetFrameFct());
      
      const Geom::Quaternion<_3D> q(GetLink().GetPart<N>().GetQuaternion(), 
				    GetLink().GetBody<N>().GetFrameFct());
      
      const Geom::Frame<_3D> f(p, q);

      return chrono::ChFrame<>(f.ToChCoordsys());
    }

  }
}


namespace OpenMeca
{  
  namespace Core
  {
    
    template<> 
    inline void
    ItemCommonProperty< OpenMeca::Item::LinkT<OpenMeca::Item::RackPinion> >::BuildIconSymbol()
    {
      OpenMeca::Item::LinkT<OpenMeca::Item::RackPinion>::BuildIconSymbol(iconSymbol_);
    }

    template<>
    inline void
    UserItemCommonProperty<OpenMeca::Item::LinkT<OpenMeca::Item::RackPinion> >::CreateAction_Specialized() 
    {
      OpenMeca::Item::Link_CreateAction_SpecializedT(*this);
    }

  }
}


#endif
