// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Link_All_hpp
#define OpenMeca_Item_Link_All_hpp

#include "OpenMeca/Item/Link/Revolute.hpp"
#include "OpenMeca/Item/Link/Motor.hpp"
#include "OpenMeca/Item/Link/LinearMotor.hpp"
#include "OpenMeca/Item/Link/Slider.hpp"
#include "OpenMeca/Item/Link/Cylindrical.hpp"
#include "OpenMeca/Item/Link/Planar.hpp"
#include "OpenMeca/Item/Link/PointLine.hpp"
#include "OpenMeca/Item/Link/PointPlane.hpp"
#include "OpenMeca/Item/Link/Spherical.hpp"
#include "OpenMeca/Item/Link/Screw.hpp"
#include "OpenMeca/Item/Link/Gear.hpp"
#include "OpenMeca/Item/Link/Pulley.hpp"
#include "OpenMeca/Item/Link/RackPinion.hpp"
#include "OpenMeca/Item/Link/Spring.hpp"

#endif
