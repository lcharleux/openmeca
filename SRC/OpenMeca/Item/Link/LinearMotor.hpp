// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Link_LinearMotor_hpp
#define OpenMeca_Item_Link_LinearMotor_hpp

#include <string>
#include <QIcon>
#include <QColor>

#include "OpenMeca/Item/Link/LinkTypeBase.hpp"
#include "OpenMeca/Gui/Dialog/DialogLinkLinearMotor.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"
#include "OpenMeca/Item/LinkT.hpp"
#include "OpenMeca/Item/Link_CreateAction_SpecializedT.hpp"

#include "OpenMeca/Util/CustomChFunction.hpp"

#include "ChronoEngine/physics/ChLinkEngine.h"
#include "ChronoEngine/physics/ChLinkEngine.h"


namespace OpenMeca
{    
  namespace Item
  {

    class LinearMotor: public LinkTypeBase
    {
    public:
      static const std::string GetStrType();
      static const QString GetQStrType();
      static bool EnableMotionLaw();


      typedef chrono::ChLinkLockLock ChLink; 
      typedef Gui::DialogLinkLinearMotor Dialog;

    public:
      LinearMotor(Link&);
      ~LinearMotor();

      template <int N> void DrawPart();
      template <int N> void BuildPoints(Core::SetOf<PartPoint>&, Core::DrawableUserItem&);

      // Accessors
      OMC_ACCESSOR(Displacement, Util::CustomChFunction,  displacement_);

    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);
    
    private:
        Util::CustomChFunction displacement_;
    };


    template<class Archive>
    inline void
    LinearMotor::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(LinkTypeBase);
      ar & BOOST_SERIALIZATION_NVP(displacement_);
    }


  }
}

namespace OpenMeca
{  
  namespace Core
  {
    

    template<> 
    inline void
    ItemCommonProperty< OpenMeca::Item::LinkT<OpenMeca::Item::LinearMotor> >::BuildIconSymbol()
    {
      OpenMeca::Item::LinkT<OpenMeca::Item::LinearMotor>::BuildIconSymbol(iconSymbol_);
    }

    template<>
    inline void
    UserItemCommonProperty<OpenMeca::Item::LinkT<OpenMeca::Item::LinearMotor> >::CreateAction_Specialized() 
    {
      OpenMeca::Item::Link_CreateAction_SpecializedT(*this);
    }

  }
}

#include <boost/serialization/version.hpp>
BOOST_CLASS_VERSION(OpenMeca::Item::LinearMotor, 0)



#endif
