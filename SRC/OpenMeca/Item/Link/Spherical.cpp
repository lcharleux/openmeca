// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/Link/Spherical.hpp" 
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/Drawable.hpp"
#include "OpenMeca/Core/System.hpp"

#include "OpenMeca/Core/Drawable.hpp" 
#include "OpenMeca/Item/PartPoint.hpp" 
#include "OpenMeca/Util/Draw.hpp"


namespace OpenMeca
{  
  namespace Item
  {

    const std::string 
    Spherical::GetStrType()
    {
      return "Spherical";
    }

    const QString 
    Spherical::GetQStrType()
    {
      return QObject::tr("Spherical");
    }

    Spherical::Spherical(Link& link)
      :LinkTypeBase(link)
    {
    }

    Spherical::~Spherical()
    {
    }

    template<> 
    void 
    Spherical::DrawPart<1>()
    {
      const double scale = Part::GetScaleValue();
      static GLUquadric* quadric = gluNewQuadric();
      glRotatef(90.0, 0.0f, 1.0f, 0.0f);
      Util::Draw::PartialSphere(0.2f*scale, 30, 0, 0, M_PI, -M_PI/2., M_PI/2.);
      glRotatef(180.0, 0.0f, 1.0f, 0.0f);
      gluDisk(quadric, 0.0f, 0.2f*scale, 30, 1);
    }

    

    template<>
    void 
    Spherical::BuildPoints<1>(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      PartPoint* p2 = new PartPoint(item);
      p1->GetName() = "center";
      p1->SetScaleCoordinate(0., 0., 0.);
      p2->GetName() = "up";
      p2->SetScaleCoordinate(0.2, 0., 0.);
      set.AddItem(*p1);
      set.AddItem(*p2);
    }


    template<> 
    void 
    Spherical::DrawPart<2>()
    {
      const double scale = Part::GetScaleValue();
      Util::Draw::Sphere(0.15f*scale);
    }


    template<>
    void 
    Spherical::BuildPoints<2>(Core::SetOf<PartPoint>& set, Core::DrawableUserItem& item)
    {
      OMC_ASSERT_MSG(set.GetTotItemNumber()==0, "The number of point is not null");
      PartPoint* p1 = new PartPoint(item);
      PartPoint* p2 = new PartPoint(item);
      p1->GetName() = "center";
      p1->SetScaleCoordinate(0., 0., 0.);
      p2->GetName() = "up";
      p2->SetScaleCoordinate(-0.15, 0., 0.);
      set.AddItem(*p1);
      set.AddItem(*p2);
    }


  }
}




