// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QSvgRenderer>

#include "OpenMeca/Item/Variable.hpp"
#include "OpenMeca/Util/Color.hpp"
#include "OpenMeca/Core/UserRootItemCommonProperty.hpp"


#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Item::Variable)

namespace OpenMeca
{  
  namespace Item
  {


    void
    Variable::Init()
    {
      Core::Singleton< Core::UserRootItemCommonProperty<Variable> >::Get().CreateAction_All();
    }
     
  

      
    Variable::Variable()
      :Core::UserRootItem(Variable::GetStrType(), Core::Singleton< Core::UserRootItemCommonProperty<Variable> >::Get().GetRootTreeWidgetItem()),
       Util::Var()
    {
    }

    Variable::~Variable()
    {
    }

    void 
    Variable::UpdateIcon()
    {
      GetIcon() = Util::Icon::DrawIconFromSvgFile(":/Rsc/Img/Equation.svg");
    }


  }

}


