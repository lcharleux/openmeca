// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/Part.hpp"
#include "OpenMeca/Core/System.hpp"

#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Item::Part)

namespace OpenMeca
{  
  namespace Item
  {

    const std::string Part::GetStrType() 
    {
      return "Part";
    } 

    const QString Part::GetQStrType() 
    {
      return  QObject::tr("Part");
    } 

    double 
    Part::GetScaleValue()
    {
      return Core::System::Get().GetScales().GetScaleValue(Part::GetStrType());
    }

    Part::~Part()
    {
    }

    Part::Part(const std::string strType, QTreeWidgetItem& parent, bool isSelectable)
    :Core::DrawableUserItem(strType, parent, isSelectable)
    {
    }


    Geom::Point<_3D>& 
    Part::GetCenter() 
    {
      OMC_ASSERT_MSG(0, "This method is not allowed with base class");
      return *(Geom::Point<_3D>*)(0);
    }

    const Geom::Point<_3D>& 
    Part::GetCenter() const 
    {
      OMC_ASSERT_MSG(0, "This method is not allowed with base class");
      return *(Geom::Point<_3D>*)(0);
    }

    Geom::Quaternion<_3D>& 
    Part::GetQuaternion()
    {
      OMC_ASSERT_MSG(0, "This method is not allowed with base class");
      return *(Geom::Quaternion<_3D>*)(0);
    }

    const Geom::Quaternion<_3D>& 
    Part::GetQuaternion() const 
    {
      OMC_ASSERT_MSG(0, "This method is not allowed with base class");
      return *(Geom::Quaternion<_3D>*)(0);
    }


  }
}




