// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef _OpenMeca_Item_Load_hpp_
#define _OpenMeca_Item_Load_hpp_


#include "OpenMeca/Item/Physical.hpp"

namespace OpenMeca
{
  namespace Item
  {


    class Load : public Physical, public Core::AutoRegister<Load>
    {

    public:
      static const std::string GetStrType(); 
      static const QString GetQStrType(); 
   
    public:
      Load(const std::string strType, QTreeWidgetItem& parentTreeItem);
      virtual ~Load();

      virtual void Apply() = 0;
      bool IsLoad() const;
      bool IsSensor() const;

    private:
      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    protected:
      Gui::SecondaryTreeItem* treeItem_;

    };
    

    template<class Archive>
    inline void
    Load::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Physical);
    }

  }
} 

#endif
