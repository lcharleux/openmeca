// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef OpenMeca_Item_PartUser_hpp_11
#define OpenMeca_Item_PartUser_hpp_11

#include "OpenMeca/Item/Part.hpp"
#include "OpenMeca/Core/AutoRegisteredPtr.hpp"



namespace OpenMeca
{  
  namespace Item
  {

    // A partUser is a part that is managed by the user.
    class PartUser: public Part
    {
    public:
      static const std::string GetStrType(); 

    public:
      PartUser(const std::string strType, Core::UserItem& parent);
      virtual ~PartUser();

      void BodyIsDeleted();

      // Accessor
      OMC_ACCESSOR(Frame      , Geom::Frame<_3D>      ,  frame_     );
      OMC_ACCESSOR(Point      , Geom::Point<_3D>      ,  center_    );
      OMC_ACCESSOR(Center     , Geom::Point<_3D>      ,  center_    );
      OMC_ACCESSOR(Quaternion , Geom::Quaternion<_3D> ,  quaternion_);

      Core::UserItem& GetParentItem();
      const Core::UserItem& GetParentItem() const;

      virtual const Geom::Frame<_3D>& GetReferenceFrame() const;

      Body& GetBody();
      const Body& GetBody() const;
      
      const Util::Color& GetColor() const;
      Util::Color& GetColor();

      const Core::AutoRegisteredPtr<Core::UserItem, PartUser >& GetParentItemPtr() const;
      Core::AutoRegisteredPtr<Core::UserItem, PartUser >& GetParentItemPtr();


    private:
      PartUser(); //Not allowed, just for serialization
      PartUser(const PartUser&);             //Not Allowed
      PartUser& operator=(const PartUser&);  //Not Allowed

      QTreeWidgetItem& GetParentTreeItem();

      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive& ar, const unsigned int);


    private:
      Core::AutoRegisteredPtr<Core::UserItem, PartUser > parentItem_;
      Geom::Point<_3D> center_;
      Geom::Quaternion<_3D> quaternion_;
      Geom::Frame<_3D> frame_;
      bool bodyIsDeleted_;
    };


    template<class Archive>
    inline void 
    PartUser::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Part);
      ar & BOOST_SERIALIZATION_NVP(parentItem_);
      ar & BOOST_SERIALIZATION_NVP(center_);
      ar & BOOST_SERIALIZATION_NVP(quaternion_);
      }
    


  }

}


#endif
