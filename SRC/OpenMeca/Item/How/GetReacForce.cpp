// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.




#include "OpenMeca/Item/How/GetReacForce.hpp"
#include "OpenMeca/Item/Body.hpp"

#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(OpenMeca::Item::How::GetReacForce::MySensor)




namespace OpenMeca
{
  namespace Item
  {
    namespace How
    {
      
      const std::string 
      GetReacForce::GetStrType()
      {
	return Physic::Force::GetStrType();
      }

      const QString
      GetReacForce::GetQStrType()
      {
	return Physic::Force::GetQStrType();
      }

      Geom::Vector<_3D> 
      GetReacForce::Acquire(MySensor& sensor)
      {
	Link& link  =  sensor.GetParent();
	chrono::ChVector<> v_loc = link.GetChLink().Get_react_force ();
	chrono::ChVector<> v_abs = link.GetChLink().GetLinkRelativeCoords().TrasformLocalToParent(v_loc);

	return Geom::Vector<_3D>(v_abs);

      }

    }
  }
}
    
   

    

