// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef _OpenMeca_Item_How_GetLinAcc_hpp_
#define _OpenMeca_Item_How_GetLinAcc_hpp_


#include "OpenMeca/Item/SensorT.hpp"
#include "OpenMeca/Item/PartPoint.hpp"
#include "OpenMeca/Physic/LinearAcceleration.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"

namespace OpenMeca
{
  namespace Item
  {
    namespace How
    {

      struct GetLinAcc
      {
	typedef SensorT<PartPoint, Physic::LinearAcceleration, GetLinAcc> MySensor;
	static const std::string GetStrType(); 
	static const QString GetQStrType(); 
	static Geom::Vector<_3D> Acquire(MySensor&);
      };


    }
  }
}
    
namespace OpenMeca
{  
  namespace Core
  {
    template<>
    inline void
    ItemCommonProperty<OpenMeca::Item::How::GetLinAcc::MySensor >::BuildIconSymbol()
    {
      OpenMeca::Item::How::GetLinAcc::MySensor::DrawIcon(iconSymbol_, Qt::gray);
    }
  }
}
   

    


#endif
