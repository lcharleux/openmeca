// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef OpenMeca_Item_PartUserPoint_hpp
#define OpenMeca_Item_PartUserPoint_hpp


#include "OpenMeca/Item/PartPoint.hpp"
#include "OpenMeca/Gui/Dialog/DialogPartUserPoint.hpp"
#include "OpenMeca/Core/ItemCommonProperty.hpp"


namespace OpenMeca
{  

  namespace Item
  {

    // An user defined point that is drawn by a little sphere.
    class PartUserPoint: public PartPoint, public Core::AutoRegister<PartUserPoint> 
    {
    public:
      static const std::string GetStrType() {return std::string("Point");};
      static const QString GetQStrType() {return QObject::tr("Point");};

      static void Init();
      static void DrawIcon(QIcon&, QColor);
      typedef Gui::DialogPartUserPoint Dialog;


    public:
      PartUserPoint(Core::UserItem& parent);
      virtual ~PartUserPoint();

      virtual void Update();
      virtual void PostDraw();

      OMC_ACCESSOR (CenterExpr, Util::ExprPoint,  centerExpr_);
      OMC_ACCESSOR (Visible   , bool           ,  visible_);

    private:
      PartUserPoint();                                 //Just for serialization
      PartUserPoint(const PartUserPoint&);             //Not Allowed
      PartUserPoint& operator=(const PartUserPoint&);  //Not Allowed

      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive& ar, const unsigned int);

    private:
      Util::ExprPoint centerExpr_;
      bool visible_;

    };

    
    template<class Archive>
    void PartUserPoint::serialize(Archive& ar, const unsigned int version)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(PartPoint);
      if (version == 0)
	const_cast<Util::ExprPoint&>(centerExpr_) = GetPoint();

      if (version > 0)
	  ar & BOOST_SERIALIZATION_NVP(centerExpr_);

      if (version > 1)
	ar & BOOST_SERIALIZATION_NVP(visible_);
      
    }
  }
}



namespace OpenMeca
{  
  namespace Core
  {

    template<>
    inline void
    ItemCommonProperty<OpenMeca::Item::PartUserPoint>::BuildIconSymbol()
    {
      QIcon icon;
      OpenMeca::Item::PartUserPoint::DrawIcon(iconSymbol_, Qt::gray);
    }

  }
}



namespace boost 
{ 
  namespace serialization 
  {
    template<class Archive>
    inline void save_construct_data(Archive & ar, 
				    const OpenMeca::Item::PartUserPoint * t, 
				    const unsigned int)
    {
      const OpenMeca::Core::UserItem* parent = &t->GetParentItem();
      ar << parent;
    }
    
    template<class Archive>
    inline void load_construct_data(Archive & ar, 
				    OpenMeca::Item::PartUserPoint * t, 
				    const unsigned int)
    {
      OpenMeca::Core::UserItem* parent = 0;
      ar >> parent;
      ::new(t)OpenMeca::Item::PartUserPoint(*parent);
    }
  }
} // namespace ...



#include <boost/serialization/version.hpp>
BOOST_CLASS_VERSION(OpenMeca::Item::PartUserPoint, 2)

#endif
