// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_Shape_Sphere_hpp
#define OpenMeca_Item_Shape_Sphere_hpp

#include <QString>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include "OpenMeca/Item/Shape/ShapeBase.hpp"
#include "OpenMeca/Gui/Dialog/Shape/DialogSphere.hpp"

#include "OpenMeca/Core/ItemCommonProperty.hpp"
#include "OpenMeca/Item/PartUserShapeT.hpp"

namespace OpenMeca
{
  namespace Item
  {
    namespace Shape
    {
      class GuiSphere;

      class Sphere : public ShapeBase
      {
      public:  
	static const std::string GetStrType(); 
	static const QString GetQStrType(); 
	typedef OpenMeca::Gui::Shape::DialogSphere GuiManager;
	static const bool CollisionDetectionAllowed = true;

      public:
	Sphere(PartUser&);
	~Sphere();
	
	void Draw() const;
	void BeginDraw();

	void BuildChSystem(chrono::ChSystem&);

	// Accessors
	OMC_ACCESSOR(Radius, double,  radius_);

	
      private:
	friend class boost::serialization::access;
	template<class Archive> void serialize(Archive& ar, const unsigned int version);
	
      private:
	double radius_;
	
      }; 

    template<class Archive>
    inline void
    Sphere::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_NVP(radius_);
    }

    }
  }
}

namespace OpenMeca
{  
  namespace Core
  {
    
    template<>
    inline void
    ItemCommonProperty<OpenMeca::Item::PartUserShapeT<OpenMeca::Item::Shape::Sphere> >::BuildIconSymbol()
    {
      OpenMeca::Item::PartUserShapeT<OpenMeca::Item::Shape::Sphere>::DrawIcon(iconSymbol_, Qt::gray);
    }
  }
}



#endif
