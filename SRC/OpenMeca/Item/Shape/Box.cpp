// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QWidget>

#include "OpenMeca/Item/Shape/Box.hpp"
#include "OpenMeca/Item/PartUserShapeT.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Util/Draw.hpp"

#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(OpenMeca::Item::PartUserShapeT<OpenMeca::Item::Shape::Box>)


namespace OpenMeca
{
  namespace Item
  {
    namespace Shape
    {
    
      const std::string
      Box::GetStrType()
      {
	return "Box";
      }

      const QString
      Box::GetQStrType()
      {
	return QObject::tr("Box");
      }
      

      Box::Box(PartUser& part)
	:ShapeBase(part),
	 lx_(0.),
	 ly_(0.),
	 lz_(0.),
	 attitude_()
      {
      }
   
      Box::~Box()
      {
      }
    

      void 
      Box::BeginDraw()
      {
	Geom::Quaternion<_3D>& q = GetPart().GetQuaternion();
	q[0] = attitude_[0];
	q[1] = attitude_[1];
	q[2] = attitude_[2];
	q.GetReal() = attitude_.GetReal();
      }

      void 
      Box::Draw() const
      {
	Util::Draw::Box(lx_, ly_, lz_);
      }

      void 
      Box::BuildChSystem(chrono::ChSystem&)
      {
	
	Body& body = GetPart().GetBody();
	Geom::Frame<_3D>&(Body::*fnptr)() = &Body::GetFrame;
	std::function<const Geom::Frame<_3D>& ()> f = boost::bind(fnptr, boost::ref(body));

	Geom::Point<_3D> p(GetPart().GetPoint(), f);
	chrono::ChVector<> chp = body.ExpressPointInLocalChFrame(p);
	chrono::ChMatrix33<> chm = attitude_.ToRotationMatrix().ToChMatrix();

	chrono::ChSharedBodyPtr& chbody = body.GetChBodyPtr();
	chbody->GetCollisionModel()->AddBox(lx_/2., ly_/2., lz_/2., &chp, &chm);
	chbody->SetCollide(true);
      }
    
    }
  }
}
