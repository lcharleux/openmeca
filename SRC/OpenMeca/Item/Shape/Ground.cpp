// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QWidget>

#include "OpenMeca/Item/Shape/Ground.hpp"
#include "OpenMeca/Item/PartUserShapeT.hpp"
#include "OpenMeca/Util/Draw.hpp"

#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(OpenMeca::Item::PartUserShapeT<OpenMeca::Item::Shape::Ground>)



namespace OpenMeca
{
  namespace Item
  {
    namespace Shape
    {
    
      const std::string
      Ground::GetStrType()
      {
	return "Ground";
      }

      const QString
      Ground::GetQStrType()
      {
	return QObject::tr("Ground");
      }
      

      Ground::Ground(PartUser& part)
	: ShapeBase(part),
	  axis_(1., 0., 0., boost::bind(&PartUser::GetReferenceFrame, boost::ref(part)))
      {
      }
   
      Ground::~Ground()
      {
      }
    

      void 
      Ground::BeginDraw()
      {
	Geom::Quaternion<_3D>& q = GetPart().GetQuaternion();
	const Geom::Vector<_3D>& X = GetPart().GetReferenceFrame().GetXAxis();
	q.SetVecFromTo(X,axis_);
      }

      void 
      Ground::Draw() const
      {
	static GLUquadric* quadric = gluNewQuadric();
	const double scale = Part::GetScaleValue();
	glTranslatef(0.01f*scale, 0.0f, 0.0f);
	Util::Draw::Box(0.01f*scale, 0.15f*scale, 0.35f*scale);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glTranslatef(-0.2f*scale, 0.0f, 0.0f);
	for (unsigned int i=0; i<4; i++)
	  {
            glTranslatef(0.08*scale, 0.0f, 0.0f);
            glRotatef(45.0f, 0.0f, 1.0f, 0.0f);
            gluCylinder(quadric, 0.01*scale, 0.01*scale, 0.15*scale, 30, 1);
            glRotatef(-45.0f, 0.0f, 1.0f, 0.0f);
	  }
      }

    
    }
  }
}
