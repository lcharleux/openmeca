## This file is part of OpenMeca, an easy software to do mechanical simulation.
##
## Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
##
## Copyright (C) 2012 Damien ANDRE
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

include(./Link/Link.pro)
include(./Shape/Shape.pro)
include(./How/How.pro)

HEADERS += Item/Body.hpp \
           Item/Link.hpp \
           Item/LinkT.hpp \
           Item/Part.hpp \
           Item/PartPoint.hpp \
           Item/PartUser.hpp \
           Item/PartUserPoint.hpp \
           Item/PartUserPipe.hpp \
           Item/PartUserJunction.hpp \
           Item/PartUserShape.hpp \
           Item/PartUserShapeT.hpp \
           Item/Physical.hpp \
           Item/PartLinkT.hpp \
           Item/Load.hpp \
           Item/LoadT.hpp \
           Item/Sensor.hpp \
           Item/SensorT.hpp \
           Item/Variable.hpp \
           Item/PartPoint_CreateAction_SpecializedT.hpp



SOURCES += Item/Body.cpp \
           Item/Link.cpp \
           Item/LinkT.cpp \
           Item/Part.cpp \
           Item/PartPoint.cpp \
           Item/PartUser.cpp \
           Item/PartUserPoint.cpp \
           Item/PartUserPipe.cpp \
           Item/PartUserJunction.cpp \
           Item/PartUserShape.cpp \
           Item/PartLinkT.cpp \
           Item/Physical.cpp \
           Item/Load.cpp \
           Item/LoadT.cpp \
           Item/Sensor.cpp \             
           Item/SensorT.cpp \
           Item/Variable.cpp
