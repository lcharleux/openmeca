// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/PartUserShape.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Item/PartPoint.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/UserItemCommonProperty.hpp"
#include "OpenMeca/Core/System.hpp"

#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(OpenMeca::Item::PartUserShape)



namespace OpenMeca
{  
  namespace Item
  {


    PartUserShape::PartUserShape(const std::string& strType, PartPoint& parent)
      :PartUser(strType, parent)
    {
    }

  
    
    
    PartUserShape::~PartUserShape()
    {
    }

    const Geom::Frame<_3D>& 
    PartUserShape::GetReferenceFrame() const
    {
      return GetBody().GetFrame();
    }
	     
    PartPoint& 
    PartUserShape::GetStartPoint()
    {
      PartPoint& startPoint = dynamic_cast<PartPoint&>(GetParentItem());
      return startPoint;
    }

    const PartPoint& 
    PartUserShape::GetStartPoint() const
    {
      const PartPoint& startPoint = dynamic_cast<const PartPoint&>(GetParentItem());
      return startPoint;
    }
	     



  }
}


