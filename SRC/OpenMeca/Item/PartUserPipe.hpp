// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_PartUserPipe_hpp
#define OpenMeca_Item_PartUserPipe_hpp


#include "OpenMeca/Item/PartUser.hpp"
#include "OpenMeca/Core/AutoRegisteredPtr.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Gui/Dialog/DialogPartUserPipe.hpp"
#include "OpenMeca/Item/PartPoint.hpp"

namespace OpenMeca
{  

  namespace Item
  {

    class PartPoint;

    // A pipe is defined by a start point, a length and an axis.
    class PartUserPipe: public PartUser, public Core::AutoRegister<PartUserPipe> 
    {
    
    public:
      static const std::string GetStrType() {return std::string("Pipe");};
      static const QString GetQStrType() {return QObject::tr("Pipe");};


      static void Init();
      static void DrawIcon(QIcon&, QColor);
      typedef Gui::DialogPartUserPipe Dialog;


    public:
      PartUserPipe(PartPoint& point);
      virtual ~PartUserPipe();
      void DrawShape();
      void UpdateIcon();
      void BeginDraw();

      Core::SetOfBase<Core::UserItem> GetAssociatedSelectedItem();
      const Geom::Frame<_3D>& GetReferenceFrame() const;

      // Accessors
      OMC_ACCESSOR     (Axis     , Geom::Vector<_3D>,  axis_    );
      OMC_ACCESSOR     (Length   , double           ,  length_  );
      OMC_ACCESSOR_PTR (EndPoint , PartPoint        ,  endPoint_);

      PartPoint& GetStartPoint();
      const PartPoint& GetStartPoint() const ;

    private:
      PartUserPipe(); //Not allowed, just for serialization
      PartUserPipe(const PartUserPipe&);             //Not Allowed
      PartUserPipe& operator=(const PartUserPipe&);  //Not Allowed


      friend class boost::serialization::access;
      template<class Archive> void save(Archive& ar, const unsigned int) const;
      template<class Archive> void load(Archive& ar, const unsigned int);
      BOOST_SERIALIZATION_SPLIT_MEMBER()

    private:
      Geom::Vector<_3D> axis_;
      double length_;
      PartPoint* endPoint_;
    };

    
    template<class Archive>
    inline void 
    PartUserPipe::save(Archive& ar, const unsigned int) const
    {
      ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(PartUser);
      ar << BOOST_SERIALIZATION_NVP(axis_);
      ar << BOOST_SERIALIZATION_NVP(length_);
      ar << BOOST_SERIALIZATION_NVP(endPoint_);
    }
    
    template<class Archive>
    inline void 
    PartUserPipe::load(Archive& ar, const unsigned int)
    {
      delete endPoint_;
      ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(PartUser);
      ar >> BOOST_SERIALIZATION_NVP(axis_);
      ar >> BOOST_SERIALIZATION_NVP(length_);
      ar >> BOOST_SERIALIZATION_NVP(endPoint_);
    }


  }

}


namespace boost 
{ 
  namespace serialization 
  {
    template<class Archive>
    inline void save_construct_data(Archive & ar, 
				    const OpenMeca::Item::PartUserPipe * t, 
				    const unsigned int)
    {
      const OpenMeca::Item::PartPoint* parent = &t->GetStartPoint();
      ar << parent;
    }
    
    template<class Archive>
    inline void load_construct_data(Archive & ar, 
				    OpenMeca::Item::PartUserPipe * t, 
				    const unsigned int)
    {
      OpenMeca::Item::PartPoint* parent = 0;
      ar >> parent;
      ::new(t)OpenMeca::Item::PartUserPipe(*parent);
    }
  }
} // namespace ...

#endif
