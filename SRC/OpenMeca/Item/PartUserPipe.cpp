// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Item/PartUserPipe.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Item/PartPoint.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/UserItemCommonProperty.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Util/Draw.hpp"

#include <boost/serialization/export.hpp>
BOOST_CLASS_EXPORT(OpenMeca::Item::PartUserPipe)



namespace OpenMeca
{  
  namespace Item
  {


    void
    PartUserPipe::Init()
    {
      Core::UserItemCommonProperty< PartUserPipe > & prop = 
	Core::Singleton< Core::UserItemCommonProperty< PartUserPipe > >::Get();

      prop.CreateAction_Edit();
      prop.CreateAction_Delete();

      // don't forget to instanciate UserRootItemCommonProperty<Body> before calling
      // create action
      Core::Singleton< Core::UserItemCommonProperty<PartPoint> >::Instanciate();
      prop.CreateAction_NewWithAutomaticSelection<PartPoint>();
    }



    void 
    PartUserPipe::DrawIcon(QIcon& icon, QColor color)
    {
      Util::Icon::DrawIconFromSvgFile(":/Rsc/Img/Part/Pipe.svg", icon, color);
    }


    
    PartUserPipe::PartUserPipe(PartPoint& parent)
      :PartUser(GetStrType(), parent),
       axis_(1.,0.,0.,boost::bind(&PartUserPipe::GetReferenceFrame, boost::ref(*this))),
       length_(0.5*Part::GetScaleValue()),
       endPoint_(new PartPoint(*this))
    {
    }

  
    
    
    PartUserPipe::~PartUserPipe()
    {
    }

    const Geom::Frame<_3D>& 
    PartUserPipe::GetReferenceFrame() const
    {
      return GetBody().GetFrame();
    }
	     

    PartPoint& 
    PartUserPipe::GetStartPoint()
    {
      PartPoint& startPoint = dynamic_cast<PartPoint&>(GetParentItem());
      return startPoint;
    }

    const PartPoint& 
    PartUserPipe::GetStartPoint() const
    {
      const PartPoint& startPoint = dynamic_cast<const PartPoint&>(GetParentItem());
      return startPoint;
    }
	     

    Core::SetOfBase<Core::UserItem> 
    PartUserPipe::GetAssociatedSelectedItem()
    {
      Core::SetOfBase<Core::UserItem> set;
      set.AddItem(GetEndPoint());
      return set;
    }

    void 
    PartUserPipe::BeginDraw()
    {
      Geom::Point<_3D>& p = PartUser::GetPoint();
      p = Geom::Point<_3D>(GetStartPoint().GetPoint(),
			   boost::bind(&PartUserPipe::GetReferenceFrame, boost::ref(*this)));

      Geom::Quaternion<_3D>& q = PartUser::GetQuaternion();
      const Geom::Vector<_3D>& X = GetReferenceFrame().GetXAxis();
      q.SetVecFromTo(X,axis_);

      GetEndPoint().GetPoint()[0] = length_;
    }

    void 
    PartUserPipe::DrawShape()
    {
      const double scale = Part::GetScaleValue();
      Util::Draw::Cylinder(.01f*scale, length_, false);
    }

    void 
    PartUserPipe::UpdateIcon()
    {
      PartUserPipe::DrawIcon(GetIcon(), GetColor().GetQColor());
    }
    
  }
}


namespace OpenMeca
{  
  namespace Core
  {

    template<>
    void
    ItemCommonProperty<OpenMeca::Item::PartUserPipe>::BuildIconSymbol()
    {
      QIcon icon;
      OpenMeca::Item::PartUserPipe::DrawIcon(iconSymbol_, Qt::gray);
    }



  }
}


