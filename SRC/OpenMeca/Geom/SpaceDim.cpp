// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


// This source file was inspired of the "libGeometrical" from 
// the GranOO workbench : http://www.granoo.org


#include "OpenMeca/Geom/SpaceDim.hpp"

namespace OpenMeca
{
  namespace Geom 
  {
    template<> std::string SpaceDimUtil<_0D>::GetStrKey(){return std::string("0D");}
    template<> std::string SpaceDimUtil<_1D>::GetStrKey(){return std::string("1D");}
    template<> std::string SpaceDimUtil<_2D>::GetStrKey(){return std::string("2D");}
    template<> std::string SpaceDimUtil<_3D>::GetStrKey(){return std::string("3D");}
  } 
}

