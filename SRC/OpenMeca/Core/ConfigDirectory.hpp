// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_ConfigDirectory_hpp
#define OpenMeca_Core_ConfigDirectory_hpp

#include <QDir>


namespace OpenMeca
{
  namespace Core
  {


    // The config directory manager class. By default it is $USER/openmeca
    class ConfigDirectory
    {
    
    public:
      static const QDir& Get();
      static bool Delete();

    private:
      static QString GetConfigDirectoryName();
      static QDir dir_;
      
      

    private:
      ConfigDirectory();                                //Not Allowed    
      ConfigDirectory(const ConfigDirectory&);            //Not Allowed    
      ConfigDirectory& operator=(const ConfigDirectory&); //Not Allowed


    };

    

    
  }
}


#endif
