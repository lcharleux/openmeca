// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_SystemSettingT_hpp
#define OpenMeca_Core_SystemSettingT_hpp

#include <QFile>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/static_assert.hpp>
#include "OpenMeca/Core/SystemSetting.hpp"
#include "OpenMeca/Util/Color.hpp"
#include "OpenMeca/Util/Icon.hpp"

namespace OpenMeca
{
  namespace Core
  {

    // Template class that allows static inheritence
    template<class T>
    class SystemSettingT : public SystemSetting
    {

    public:
      static const QIcon GetIcon();
      static const std::string GetStrType();
      static T& Get();

    public:    
      SystemSettingT();
      virtual ~SystemSettingT();
      
    private :
      SystemSettingT(const SystemSettingT&);             //Not Allowed
      SystemSettingT& operator=(const SystemSettingT&);  //Not Allowed

           // - BOOST SERIALIZATION - //
      friend class boost::serialization::access;
      template<class Archive> void save(Archive&, const unsigned int) const;
      template<class Archive> void load(Archive&, const unsigned int); 
      BOOST_SERIALIZATION_SPLIT_MEMBER()     

    private:
      static SystemSettingT<T>* me_;

    }; 

    template<class T> SystemSettingT<T>* SystemSettingT<T>::me_ = 0;

    template<class T>
    template<class Archive>
    void 
    SystemSettingT<T>::save(Archive& ar, const unsigned int ) const
    {
      ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(SystemSetting);
    }

    template<class T>
    template<class Archive>
    void 
    SystemSettingT<T>::load(Archive& ar, const unsigned int ) 
    {
      ar >> BOOST_SERIALIZATION_BASE_OBJECT_NVP(SystemSetting);
      me_ = this;
    }

    

    template<class T>
    inline const QIcon
    SystemSettingT<T>::GetIcon()
    {
      const QString file = ":/Rsc/Img/SystemSetting/" + QString(T::GetStrType().c_str()) + ".svg";
      return Util::Icon::DrawIconFromSvgFile(file);
    }

    template<class T>
    inline const std::string 
    SystemSettingT<T>::GetStrType()
    {
      return T::GetStrType();
    }
    
    template<class T>
    inline T& 
    SystemSettingT<T>::Get()
    {
      BOOST_STATIC_ASSERT_MSG((boost::is_base_of<SystemSettingT<T>, T>::value),
			      "T must be a descendant of Singleton");
      if (me_ == 0)
	new T();
      OMC_ASSERT_MSG(me_ != 0, "The singleton is not built");
      return static_cast<T&>(*me_);
    }
    

    template<class T>
    inline
    SystemSettingT<T>::SystemSettingT()
      :SystemSetting(T::GetStrType())
    {
      Item::icon_ = GetIcon();
      GetMainTreeItem().setIcon(0, Item::icon_);
      me_ = this;
    }
    
    template<class T>
    inline
    SystemSettingT<T>::~SystemSettingT()
    {
      if (me_ == this)
	me_ = 0;
    }

  }
}
#endif
