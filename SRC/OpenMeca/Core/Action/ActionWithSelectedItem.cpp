// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Core/Action/ActionWithSelectedItem.hpp"


namespace OpenMeca
{
  namespace Core
  {
    Core::SetOf<ActionWithSelectedItem> ActionWithSelectedItem::all_ = 
      Core::SetOf<ActionWithSelectedItem>();

    ActionWithSelectedItem::ActionWithSelectedItem(const QIcon & icon, 
						   const QString & text,
						   const std::string& id)
      :Action(icon, text, id)
    {
      QAction::setEnabled(false);
      all_.AddItem(*this);
    }

    ActionWithSelectedItem::~ActionWithSelectedItem()
    {
      all_.RemoveItem(*this);
    }

    void
    ActionWithSelectedItem::Enable(Core::Item& item)
    {
      Core::SetOf<ActionWithSelectedItem>::it it;
      for ( it=all_.Begin() ; it != all_.End(); it++ )
	(*it)->setEnabled((*it)->IsMyType(item));
    }

     void
    ActionWithSelectedItem::DisableAll()
    {
      Core::SetOf<ActionWithSelectedItem>::it it;
      for ( it=all_.Begin() ; it != all_.End(); it++ )
	(*it)->setEnabled(false);
    }
    
    
  }
}

