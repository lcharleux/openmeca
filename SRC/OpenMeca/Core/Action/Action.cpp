// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.




#include "OpenMeca/Core/Action/Action.hpp"

namespace OpenMeca
{
  namespace Core
  {

    Action::Action(const QIcon & icon, const QString & text,  const std::string& id)
      :QAction(icon, text, &Gui::MainWindow::Get()),
       id_(id)
    {
      QAction::setIconVisibleInMenu(true);
      QObject::connect(this, SIGNAL(triggered()), this, SLOT(ActionRequired()));
    }

    Action::~Action()
    {
    }

    void
    Action::ActionRequired()
    {
      DoAction();
    }
    
    const std::string& 
    Action::GetId()
    {
      return id_;
    }


  }
}


