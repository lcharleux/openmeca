// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_Action_ActionWithSelectedItem_hpp
#define OpenMeca_Core_Action_ActionWithSelectedItem_hpp



#include "OpenMeca/Core/Action/Action.hpp"
#include "OpenMeca/Core/Item.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"

namespace OpenMeca
{
  namespace Core
  {
    class ActionWithSelectedItem : public Action, 
				   public Core::AutoRegister<ActionWithSelectedItem> 
    {
    public:    
      static void Enable(Core::Item&);
      static void DisableAll();

      ActionWithSelectedItem(const QIcon&, const QString&, const std::string&);
      virtual ~ActionWithSelectedItem();
      
      
    private:
      ActionWithSelectedItem();                                          //Not Allowed
      ActionWithSelectedItem(const ActionWithSelectedItem&);            //Not Allowed    
      ActionWithSelectedItem& operator=(const ActionWithSelectedItem&); //Not Allowed

    protected:
      virtual bool IsMyType(Core::Item&) = 0;

    private:
      static Core::SetOf<ActionWithSelectedItem> all_;      
    };
    
    
  }
}

#endif
