// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <QFile>
#include <QDomDocument>
#include <QDomNode>


#include "OpenMeca/Core/CommonProperty.hpp"
#include "OpenMeca/Core/XmlConfigFile.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"


namespace OpenMeca
{
  namespace Core
  {
    std::map<const std::string, CommonProperty* > CommonProperty::registeredClasses_ = std::map<const std::string, CommonProperty* >();

    void 
    CommonProperty::AddClass(const std::string id, CommonProperty& theClass)
    {
      OMC_ASSERT_MSG(registeredClasses_.count(id)==0, "The class is already registered");
      registeredClasses_[id] = &theClass;
    }

    CommonProperty&
    CommonProperty::GetClass(const std::string id)
    {
      OMC_ASSERT_MSG(registeredClasses_.count(id)==1, "The class is not registered");
      return *registeredClasses_[id];
    }
   

    CommonProperty::CommonProperty()
      :registeredActions_(),
       registeredPopUpActions_(),
       subMenu_(),
       popUpMenu_()
    {
    }

    CommonProperty::~CommonProperty()
    {
    }
    
    void 
    CommonProperty::AddAction(Action& action)
    {
      OMC_ASSERT_MSG(registeredActions_.count(action.text().toStdString())==0,
		 "The action is already registered");
      registeredActions_[action.GetId()] = &action;
    }

    void 
    CommonProperty::AddPopUpAction(Action& action, const std::string subMenuID)
    {
      OMC_ASSERT_MSG(registeredPopUpActions_.count(action.text().toStdString())==0,
		 "The action is already registered");
      registeredPopUpActions_[action.GetId()] = &action;
      if (subMenuID == "")
	popUpMenu_.addAction(&action);
      else
	GetSubMenu(subMenuID).addAction(&action);
    }

    void 
    CommonProperty::AddPopUpSeparator()
    {
      popUpMenu_.addSeparator();
    }

    Action& 
    CommonProperty::GetAction(const std::string id)
    {
      OMC_ASSERT_MSG(registeredActions_.count(id)==1,
		 "The action is not registered");
      return *registeredActions_[id];
    }

    QMenu&
    CommonProperty::GetPopUpMenu()
    {
      return popUpMenu_;
    }

    

    void 
    CommonProperty::AddPopUpSubMenu(const std::string& title, const QIcon & icon)
    {
      QMenu* sub = popUpMenu_.addMenu(icon, QObject::tr(title.c_str()));
      OMC_ASSERT_MSG(subMenu_.count(title)==0, "The id of this submenu is already taken");
      subMenu_[title] = sub;
    }


    QMenu& 
    CommonProperty::GetSubMenu(const std::string& title)
    {
      OMC_ASSERT_MSG(subMenu_.count(title)==1, "Can't find the required submenu with this id");
      return *subMenu_[title];
    }
  }
}

