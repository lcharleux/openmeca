// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_UserItemCommonProperty_hpp
#define OpenMeca_Core_UserItemCommonProperty_hpp


#include <QString>
#include <QIcon>

#include "OpenMeca/Core/ItemCommonProperty.hpp"
#include "OpenMeca/Core/Action/ActionNewUserItem.hpp"
#include "OpenMeca/Core/Action/ActionWithoutSelection.hpp"
#include "OpenMeca/Core/Action/ActionWithSelectedItemT.hpp"
#include "OpenMeca/Core/Action/ActionNewUserItemWithSelection.hpp"
#include "OpenMeca/Core/Action/ActionNewUserItemWithAutomaticSelection.hpp"
#include "OpenMeca/Core/Action/ActionDeleteUserItem.hpp"
#include "OpenMeca/Core/Action/ActionEditItem.hpp"

namespace OpenMeca
{
  namespace Core
  {

    // Common property for the classes that inherit from the 'UserItem' class
    // It allows to automatically create the required action like 'new', 'edit' and
    // to define the user item generic icons, etc...
    template<class T>
    class UserItemCommonProperty : public ItemCommonProperty<T>, Singleton< UserItemCommonProperty<T> >
    {
      
      friend class Singleton< UserItemCommonProperty<T> >;
    
    public:
      void CreateAction_New();
      template <class New> void CreateAction_NewWithSelection(const std::string popUpSubMenuID="");
      template <class Parent> void CreateAction_NewWithAutomaticSelection();

      void CreateAction_Edit();
      void CreateAction_Delete();
      void CreateAction_Specialized();//Specialize this to add specific item
      void CreateAction_All();
      void CreateAction_Nothing();
      void AddPopUpSeparator();

    protected:
      UserItemCommonProperty();
      virtual ~UserItemCommonProperty();

    private:
      UserItemCommonProperty(const UserItemCommonProperty<T>&);               //Not Allowed    
      UserItemCommonProperty<T>& operator=(const UserItemCommonProperty<T>&); //Not Allowed
    };


    template<class T>
    inline  
    UserItemCommonProperty<T>::UserItemCommonProperty()
      :ItemCommonProperty<T>()
    {
    }
    
    template<class T>
    inline  
    UserItemCommonProperty<T>::~UserItemCommonProperty()
    {
     
    }

    template<class T>
    inline void 
    UserItemCommonProperty<T>::CreateAction_New()
    {
      Action& newAction    = *new ActionWithoutSelection<T, ActionNewUserItem<T> >();
      CommonProperty::AddAction(newAction);
    }

    template<class T>
    template<class Parent>
    inline void 
    UserItemCommonProperty<T>::CreateAction_NewWithAutomaticSelection()
    {
      Action& newAction    = 
	*new ActionWithoutSelection<T, ActionNewUserItemWithAutomaticSelection<T,Parent> >();
      CommonProperty::AddAction(newAction);
    }


    template<class T>
    template<class New>
    inline void 
    UserItemCommonProperty<T>::CreateAction_NewWithSelection(const std::string popUpSubMenuID)
    {
      Action& newAction    = 
	*new ActionWithSelectedItemT<T, ActionNewUserItemWithSelection<T,New> >();
      CommonProperty::AddAction(newAction);
      CommonProperty::AddPopUpAction(newAction, popUpSubMenuID);
    }


    template<class T>
    inline void 
    UserItemCommonProperty<T>::CreateAction_Edit()
    {
       Action& editAction   = 
	 *new ActionWithSelectedItemT<T, ActionEditItem<T> >();
      CommonProperty::AddAction(editAction);
      CommonProperty::AddPopUpAction(editAction);
    }

    
    template<class T>
    inline void 
    UserItemCommonProperty<T>::CreateAction_Delete()
    {
      Action& deleteAction = 
	*new ActionWithSelectedItemT<T, ActionDeleteUserItem<T> >();
      CommonProperty::AddAction(deleteAction);
      CommonProperty::AddPopUpAction(deleteAction);
    }

    
    template<class T>
    inline void 
    UserItemCommonProperty<T>::AddPopUpSeparator()
    {
      CommonProperty::AddPopUpSeparator();
    }

    template<class T>
    inline void 
    UserItemCommonProperty<T>::CreateAction_Specialized()
    {
      
    }
  
    template<class T>
    inline void 
    UserItemCommonProperty<T>::CreateAction_All()
    {
      CreateAction_New();
      CreateAction_Edit();
      CreateAction_Delete();
      CreateAction_Specialized();
    }

    template<class T>
    inline void 
    UserItemCommonProperty<T>::CreateAction_Nothing()
    {
    }

  
  }
}


#endif
