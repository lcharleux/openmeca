// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_GlobalSettingCommonProperty_hpp
#define OpenMeca_Core_GlobalSettingCommonProperty_hpp


#include <QString>
#include <QIcon>

#include "OpenMeca/Core/ItemCommonProperty.hpp"

#include "OpenMeca/Core/Action/ActionWithoutSelection.hpp"
#include "OpenMeca/Core/Action/ActionEditGlobalSetting.hpp"

namespace OpenMeca
{
  namespace Core
  {

    // Common property for the classes that inherit from the 'GlobalSetting' class
    template<class T>
    class GlobalSettingCommonProperty : public ItemCommonProperty<T>, public Singleton< GlobalSettingCommonProperty<T> >
    {
      friend class Singleton< GlobalSettingCommonProperty<T> >;

    public:
      
      void RegisterAction();
      void RegisterAction_Specialized();//Specialize this to add specific item

    protected:
      GlobalSettingCommonProperty();
      virtual ~GlobalSettingCommonProperty();
    
    private:
      GlobalSettingCommonProperty(const GlobalSettingCommonProperty<T>&);               //Not Allowed    
      GlobalSettingCommonProperty<T>& operator=(const GlobalSettingCommonProperty<T>&); //Not Allowed
    };


    template<class T>
    inline  
    GlobalSettingCommonProperty<T>::GlobalSettingCommonProperty()
      :ItemCommonProperty<T>()
    {
      RegisterAction();
    }
    
    template<class T>
    inline  
    GlobalSettingCommonProperty<T>::~GlobalSettingCommonProperty()
    {
     
    }
  
    template<class T>
    inline void 
    GlobalSettingCommonProperty<T>::RegisterAction()
    {
      Action& edit = 
	*new ActionWithoutSelection<T, ActionEditGlobalSetting<T> >();
      ItemCommonProperty<T>::AddAction(edit);
    }
    
    template<class T>
    inline void 
    GlobalSettingCommonProperty<T>::RegisterAction_Specialized() 
    {
      
    }
  
  }
}


#endif
