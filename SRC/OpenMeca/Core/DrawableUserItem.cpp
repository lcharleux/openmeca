// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OSX
  #include <GL/glu.h>
#else
  #include <OpenGL/glu.h>
#endif


#include "OpenMeca/Core/DrawableUserItem.hpp"
#include "OpenMeca/Core/SelectionManager.hpp"

#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Core::DrawableUserItem)

namespace OpenMeca
{  
  namespace Core
  {

    int DrawableUserItem::drawableItemCounter_ = 0;
    std::map<int, DrawableUserItem*> DrawableUserItem::map_ = std::map<int, DrawableUserItem*>();

    DrawableUserItem& 
    DrawableUserItem::GetDrawableItemByGLKey(int itemGLKey)
    {
      OMC_ASSERT_MSG((map_.count(itemGLKey)==1), "Can't find itemGLKey");
      return *map_[itemGLKey];      
    }




    const std::string 
    DrawableUserItem::GetStrType() 
    {
      return "DrawableUserItem";
    }

    DrawableUserItem::DrawableUserItem(const std::string strType, QTreeWidgetItem& parent, bool isSelectable)
      :Core::UserItem(strType, parent),
       itemGLKey_(drawableItemCounter_),
       isSelectable_(isSelectable),
       drawLocalFrame_(false)
    {
      OMC_ASSERT_MSG(map_.count(itemGLKey_)==0, "The ItemGLKey is already used");
      map_[itemGLKey_] = this;
      drawableItemCounter_++;
    }

    DrawableUserItem::~DrawableUserItem()
    {
    }

 
    void
    DrawableUserItem::Draw(bool withName)
    {
      BeginDraw();

      if (IsSelected())
	Core::Singleton<Core::SelectionManager>::Get().GetSelectedColor().ApplyGLColor();
      else
	GetColor().ApplyGLColor();

      
      GetFrame().UpdateGLMatrix();

      glPushMatrix();

      if (withName == true && IsSelectable())
	glPushName(GetItemGLKey());

      glMultMatrixd(GetFrame().GetGLMatrix());

      if (drawLocalFrame_)
	GetFrame().Draw();      

      DrawShape();

      if (withName == true && IsSelectable())
	glPopName();

      glPopMatrix();
      
      EndDraw();
    }

    unsigned int 
    DrawableUserItem::GetItemGLKey() const
    {
      return itemGLKey_;
    }

    bool 
    DrawableUserItem::IsSelectable() const
    {
      return isSelectable_;
    }


  }
}




