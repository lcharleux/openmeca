// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_UserItem_hpp
#define OpenMeca_Core_UserItem_hpp

#include <string>
#include <QIcon>
#include <QTreeWidgetItem>

#include <boost/bind.hpp>

#include "OpenMeca/Core/Item.hpp"
#include "OpenMeca/Core/SetOf.hpp"
#include "OpenMeca/Gui/SecondaryTreeItem.hpp"
#include "OpenMeca/Geom/Frame.hpp"
#include "OpenMeca/Util/Color.hpp"




namespace OpenMeca
{
  namespace Gui
  {
    class MainTreeItem;
    class SecondaryTreeItem;
  }

  namespace Item
  {
    class Body;
  }



  namespace Core
  {
    
    // The user item base class.
    // An user item is an item that can be created, deleted, and edited by a user.
    class UserItem : public Item, public AutoRegister<UserItem>
    {
    public:    
      UserItem(const std::string strType, QTreeWidgetItem& parent);
      virtual ~UserItem();
      
      void AddChildItem(UserItem&);
      void EraseChildItem(UserItem&);

      const std::string& GetName() const;
      std::string& GetName();
      
      virtual void Update();

      const SetOf<UserItem>& GetDependentItems() const;
      SetOf<UserItem>& GetDependentItems();

      SetOfBase<UserItem> GetAllDependentItems(); // recursive computation of dependant item

      void ChangeParentTreeItem(QTreeWidgetItem& parent);

      void Select();
      void UnSelect(); 
      bool IsSelected() const;
      void SetIsSelected(bool);

      bool HasChild(const UserItem&) const;

      

      std::function<const Geom::Frame<_3D>& ()> GetFrameFct() const;

      virtual const Geom::Frame<_3D>& GetReferenceFrame() const; // forbidden
      virtual const Geom::Frame<_3D>& GetFrame() const; // forbidden
      virtual const Util::Color& GetColor() const; // forbidden
      virtual Util::Color& GetColor(); // forbidden 
      virtual const OpenMeca::Item::Body& GetBody() const; // forbidden
      virtual OpenMeca::Item::Body& GetBody(); // forbidden
    
    protected:
      virtual void UpdateIcon() = 0;
      virtual SetOfBase<UserItem> GetAssociatedSelectedItem(); //Default return empty set
      void ConcatenateDependentItems(SetOfBase<UserItem>&);

    private :
      std::string name_;
      SetOf<UserItem> dependentItems_;
      SetOf<UserItem> parentsItems_;
      

    private :
      UserItem();                            //Not Allowed
      UserItem(const UserItem&);             //Not Allowed
      UserItem& operator=(const UserItem&);  //Not Allowed
      

      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int);
      
    }; 

    template<class Archive>
    inline void
    UserItem::serialize(Archive & ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Item);
      ar & BOOST_SERIALIZATION_NVP(name_);
    }


    

  }
}


#endif
