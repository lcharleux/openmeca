// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <iostream>
#include <fstream>

#include <boost/lexical_cast.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "OpenMeca/Util/Var.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/Drawable.hpp"
#include "OpenMeca/Item/Body.hpp"
#include "OpenMeca/Item/Link.hpp"
#include "OpenMeca/Item/PartLinkT.hpp"
#include "OpenMeca/Item/PartUserPoint.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Core/Software.hpp"

namespace OpenMeca
{

  namespace Core
  {

    //************** Static member and attributes *************************
    System* System::activeSystem_ = 0;

    bool System::deleted_ = false;

    chrono::ChSystem* System::chSystem_ = 0;
  
    const float System::Version = 2.0; 

    bool
    System::IsDeleted()
    {
      return deleted_;
    }


    System& 
    System::Get()
    {
      OMC_ASSERT_MSG(IsDeleted() == false, "The system is deleted");
      if (activeSystem_==0)
    	activeSystem_ = new System();
      return *activeSystem_;
    }

    void
    System::Quit()
    {
      delete &System::Get();
      System::deleted_ = true;
    }


  
    //************** Constructors and Destructors *************************
    System::System()
      :version_("2.0"), 
       historic_(*this), 
       enableDrawing_(true),
       time_(0.)
    {
    }

    System::~System()
    {
      GetSetOf<UserRootItem>().DeleteAllItem();
      GetSetOf<SystemSetting>().DeleteAllItem();
    }
    
    void 
    System::DisableDrawing()
    {
      enableDrawing_ = false;
    }

    void 
    System::EnableDrawing()
    {
      enableDrawing_ = true;
    }

    

    bool
    System::IsEdited()
    {
      return historic_.IsSystemEdited();
    }

    void
    System::Update()
    {
      Util::Expr::UpdateAll();
      {
	Core::SetOf<UserRootItem>::it it;
	Core::SetOf<UserRootItem>& set = GetSetOf<UserRootItem>();
	for (it = set.Begin() ; it != set.End(); it++ )
	  (*it)->Update();
      }


      {
	Core::SetOf<SystemSetting>::it it;
	Core::SetOf<SystemSetting>& set = GetSetOf<SystemSetting>();
	for (it = set.Begin() ; it != set.End(); it++ )
	  (*it)->Update();
      }

      {
	
      }

      Gui::MainWindow::Get().GetViewer().updateGL(); 
    }

    HistoricManager& 
    System::GetHistoric()
    {
      return historic_;
    }
    

    const HistoricManager& 
    System::GetHistoric() const
    {
      return historic_;
    }
    


    void
    System::Save(std::string& str)
    {
      std::stringstream ss;
      boost::archive::text_oarchive oa(ss);
      oa << *this; 
      str = ss.str();
    }
    

    void
    System::Load(const std::string& str)
    {      
      Reset();
      std::stringstream ss;
      ss << str;
      boost::archive::text_iarchive ia(ss);
      ia >> *this;
    }


    void
    System::SaveFile(const std::string& str)
    {
      std::ofstream ofs(str.c_str());
      if (!ofs.is_open())
	{
	  QString msg = QObject::tr("Can't save file") + " \"";
	  msg += QString(str.c_str()) + "\"";
	  OMC_WARNING_MSG(msg);
	  return;
	}

      boost::archive::text_oarchive oa(ofs);
      oa << *this;
      historic_.UserSave();
    }
    
    void
    System::LoadFile(const std::string& str)
    {    
      Reset();
      std::ifstream ifs(str.c_str());
      if (!ifs.is_open())
	{
	  QString msg = QObject::tr("Can't open file") + " \"";
	  msg += QString(str.c_str()) + "\"";
	  OMC_WARNING_MSG(msg);
	  return;
	}
      boost::archive::text_iarchive ia(ifs);
      ia >> *this;
      historic_.Reset();
      Update();
    }

    void
    System::Reset()
    {
      Singleton<SelectionManager>::Get().SetNoItemSelected();
      GetSetOf<UserRootItem>().ClearAndDelete();
      OMC_ASSERT_MSG(GetSetOf<UserRootItem>().GetTotItemNumber() == 0,
		 "The user root item set must be empty after system reset");
      OMC_ASSERT_MSG(GetSetOf<UserItem>().GetTotItemNumber() == 0,
		 "The user item set must be empty after system reset");
    }

    void
    System::Draw() const
    {
      if (enableDrawing_)
	{
	  SetOf<Drawable>::const_it it;
	  for (it=GetSetOf<Drawable>().Begin() ; it != GetSetOf<Drawable>().End(); it++ )
	    (*it)->Draw();

	  typedef OpenMeca::Item::Body Body;
	  SetOf<Body>::const_it it_body;
	  for (it_body=GetSetOf<Body>().Begin() ; it_body != GetSetOf<Body>().End(); it_body++ )
	    (*it_body)->Draw();

	  glDisable(GL_LIGHTING);
	  glDisable(GL_DEPTH_TEST);
	  for (it=GetSetOf<Drawable>().Begin() ; it != GetSetOf<Drawable>().End(); it++ )
	    (*it)->PostDraw();
	  glEnable(GL_DEPTH_TEST);
	  glEnable(GL_LIGHTING);
	}

      
      

      
    }

    void
    System::DrawWithNames() const
    {
      if (enableDrawing_)
	{
	  SetOf<Drawable>::const_it it;
	  for (it=GetSetOf<Drawable>().Begin() ; it != GetSetOf<Drawable>().End(); it++ )
	    (*it)->Draw(true);
	}
    }
    


    std::string
    System::GetAutomaticName(const std::string& childTypeStr) const
    {
      int maxNumber = 0;
      SetOf<UserItem>::const_it it;
      for (it=GetSetOf<UserItem>().Begin() ; it != GetSetOf<UserItem>().End(); it++ )
	{
	  std::size_t size = (*it)->GetName().find(childTypeStr);
	  if (size != std::string::npos)
	    {
	      if ((*it)->GetName().size() >= childTypeStr.size() + 2)
		{
		  std::string copy = (*it)->GetName();
		  copy.erase(0, childTypeStr.size() + 1);
		  int num = atoi (copy.c_str());
		  if (num > maxNumber)
		    maxNumber = num;
		}
	    }
	}
      maxNumber++;
      return childTypeStr + '_' + boost::lexical_cast<std::string>(maxNumber);
    }

    OpenMeca::Setting::Scales& 
    System::GetScales()
    {
      return SystemSettingT<OpenMeca::Setting::Scales>::Get();
    }
    
    const OpenMeca::Setting::Scales& 
    System::GetScales () const
    {
      return SystemSettingT<OpenMeca::Setting::Scales>::Get();
    }

    chrono::ChSystem& 
    System::GetChSystem()
    {
      if (chSystem_ == 0)
	chSystem_ = new chrono::ChSystem();
      return *chSystem_;
    }


    void 
    System::BuildChSystem()
    {
      chrono::ChSystem& chSystem = GetChSystem();
      chSystem.Clear();

      Core::SetOf<Item>::it it;
      
      for (it = GetSetOf<Item>().Begin() ; it != GetSetOf<Item>().End(); it++ )
	(*it)->InitChSystem(chSystem);
      
      for (it = GetSetOf<Item>().Begin() ; it != GetSetOf<Item>().End(); it++ )
	(*it)->BuildChSystem(chSystem);

      time_ = 0.;
      
    }

    void 
    System::UpdateValueFromCh()
    {
      Core::SetOf<UserItem>::it it;
      for (it = GetSetOf<UserItem>().Begin() ; it != GetSetOf<UserItem>().End(); it++ )
	(*it)->UpdateValueFromCh();

      time_ = GetChSystem().GetChTime();
    }

    void 
    System::SaveState()
    {
      Core::SetOf<UserItem>::it it;
      for (it = GetSetOf<UserItem>().Begin() ; it != GetSetOf<UserItem>().End(); it++ )
	(*it)->SaveState();
    }

    void 
    System::ResetState()
    {
      Core::SetOf<UserItem>::it it;
      for (it = GetSetOf<UserItem>().Begin() ; it != GetSetOf<UserItem>().End(); it++ )
	(*it)->ResetState();      
    }
    
    void 
    System::RecoveryState(unsigned int i)
    {
      Core::SetOf<UserItem>::it it;
      for (it = GetSetOf<UserItem>().Begin() ; it != GetSetOf<UserItem>().End(); it++ )
	(*it)->RecoveryState(i);
    }


    void
    System::ManageLoadedFileVersion(const Util::Version& archiveVersion)
    {
      if ((GetSoftwareVersion() == archiveVersion) == false)
	{
	  OMC_ASSERT_MSG(GetSoftwareVersion() > archiveVersion,
			 "the file version must be lower than the system version");
	}

      // Display message or not ? The answer is no !!!
      // if (archiveVersion < GetSoftwareVersion())
      // 	{
      // 	  QString text = QObject::tr("You are loading a file created with an") + " ";
      // 	  text += QObject::tr("older version of openmeca") + ".\n";

      // 	  text += QObject::tr("Note that your file version is") + " ";
      // 	  text += QString(archiveVersion.ToString().c_str()) + " ";
      // 	  text += QObject::tr("and the software version is") + " ";
      // 	  text += QString(GetSoftwareVersion().ToString().c_str());
      // 	  text += ".";
      // 	  OMC_INFO_MSG(text);
      // 	}
    }

    const Util::Version& 
    System::GetSoftwareVersion() const
    {
      return Core::Software::Get().Version();
    }


  }
}





