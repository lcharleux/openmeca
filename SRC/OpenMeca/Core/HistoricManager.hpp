// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_HistoricManager_hpp
#define OpenMeca_Core_HistoricManager_hpp

#include <string>
#include <vector>

namespace OpenMeca
{
  namespace Core
  {

    class System;
    
    // The historic manager class that manages the undo/redo feature
    class HistoricManager
    {
    public:
      HistoricManager(System& system);
      ~HistoricManager();
      
      void LoadPreviousState();
      void LoadNextState();
      void Reset();
      bool IsUndoPossible() const;
      bool IsRedoPossible() const;
      bool IsSystemEdited() const;
      void SystemEdited();
      void UserSave();

    private:
      void Init();
      bool IsInUndoRedoMode();
      
      HistoricManager();                          //Not Allowed
      HistoricManager(const HistoricManager&);             //Not Allowed
      HistoricManager& operator=(const HistoricManager&);  //Not Allowed

      private :
      System& system_;
      std::vector< std::string > archives_;
      std::vector< std::string >::iterator historicNavigator_;
      std::string lastUserSavedArchive_;
    };
    
    
  }
}
#endif
