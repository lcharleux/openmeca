// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_Macro_hpp
#define OpenMeca_Core_Macro_hpp


// This is a macro to autmatic access to class member, for example the macro
// OMC_ACCESSOR(Mass, double, mass_); -> will give the 4 members fonctions
//    -        double& GetMass()       {return mass_};
//    -  const double& GetMass() const {return mass_};
//
// Note that these functions are inlined !


#define OMC_ACCESS(method, type, variable)    \
  type & Get##method () {return variable;}    

#define OMC_ACCESS_CST(method, type, variable) \
  const type & Get##method () const {return variable;}                  

#define OMC_ACCESSOR(method, type, variable) \
  OMC_ACCESS      (method, type, variable)   \
  OMC_ACCESS_CST(method, type, variable)



#define OMC_ACCESS_PTR(method, type, variable)    \
  type & Get##method () {assert(variable!=0);return *variable;}    

#define OMC_ACCESS_CST_PTR(method, type, variable) \
  const type & Get##method () const {assert(variable!=0);return *variable;}                  

#define OMC_ACCESSOR_PTR(method, type, variable)  \
  OMC_ACCESS_PTR      (method, type, variable)	  \
  OMC_ACCESS_CST_PTR(method, type, variable) 



// This macro can replace the standard 'assert' macro.
// If the assertion is raised, it stop the program and prints the given message.
#ifndef NDEBUG
#   define OMC_ASSERT_MSG(condition, message) \
    do { \
        if (! (condition)) { \
	  OpenMeca::Core::RaiseAssertion(__LINE__,  __FILE__, #condition, message); \
        }   \
    } while (false)
#else
#   define OMC_ASSERT_MSG(condition, message) do { } while (false)
#endif

#ifndef NDEBUG
#   define OMC_ASSERT(condition) \
    do { \
        if (! (condition)) { \
	  OpenMeca::Core::RaiseAssertion(__LINE__,  __FILE__, #condition, ""); \
        }   \
    } while (false)
#else
#   define OMC_ASSERT(condition, message) do { } while (false)
#endif


#define OMC_INFO_MSG(message)  \
  OpenMeca::Core::DisplayInfoMsg(message)

#define OMC_WARNING_MSG(message)  \
  OpenMeca::Core::DisplayWarningMsg(message)

#include <string>
#include <QString>

namespace OpenMeca
{
  namespace Core
  {
    
    void RaiseAssertion(int line, const std::string& file, const std::string& condition, const std::string& message);

    void DisplayInfoMsg(const QString& message);
    void DisplayWarningMsg(const QString& message);
  }
}


#endif
