// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Core/SelectionManager.hpp"
#include "OpenMeca/Core/DrawableUserItem.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"

#include "OpenMeca/Core/Action/ActionWithSelectedItem.hpp"

namespace OpenMeca
{
  namespace Core
  {

    SelectionManager::SelectionManager()
      :QObject(),
       itemSelected_(0),
       selectedColor_(Qt::yellow)
    {
    }

    SelectionManager::~SelectionManager()
    {
    }
      
    void 
    SelectionManager::SetItemSelected(Item& item)
    {
      Item::UnselectAll();
      itemSelected_ = &item;
      itemSelected_->Select();
      ActionWithSelectedItem::Enable(item);
      emit ItemSelected(item);
      Gui::MainWindow::Get().GetViewer().updateGL(); 
    }

    void 
    SelectionManager::SetDrawableItemSelected(int i)
    {
      DrawableUserItem& drawableItem = DrawableUserItem::GetDrawableItemByGLKey(i);
      Gui::MainWindow::Get().GetTreeView().setCurrentItem(&drawableItem.GetMainTreeItem(), 0);
      SetItemSelected(drawableItem);
    }


    void  
    SelectionManager::SetNoItemSelected()
    {
      itemSelected_ = 0;
      ActionWithSelectedItem::DisableAll();
      Item::UnselectAll();
      Gui::MainWindow::Get().GetTreeView().setCurrentItem(0);
      Gui::MainWindow::Get().GetViewer().updateGL(); 
    }

    bool 
    SelectionManager::IsItemSelected()
    {
      return itemSelected_ != 0;
    }

    Item& 
    SelectionManager::GetItemSelected()
    {
      return GetItem();
    }
    
    Item& 
    SelectionManager::GetItem()
    {
      OMC_ASSERT_MSG(itemSelected_ != 0, "The selected item is null");
      return *itemSelected_;
    }

    const Util::Color&
    SelectionManager::GetSelectedColor() const
    {
      return selectedColor_;
    }

  }
}
 

