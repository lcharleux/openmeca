// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_ConfigFile_hpp
#define OpenMeca_Core_ConfigFile_hpp

#include <QFile>
#include <QDir>
#include <map>

namespace OpenMeca
{
  namespace Core
  {


    // Base class that manages Xml config files used to save/load  global settings.
    class ConfigFile
    {
    
    public:
      static void RestoreAll();
      
      ConfigFile(const std::string name, const std::string directory);      
      virtual ~ConfigFile(); 
      void RestoreBackup();
      QFile& Open();
      QFile& GetBackupFile();
      QDir GetConfigFileDirectory();
      QString GetAbsPath();
    private:
      ConfigFile();                                //Not Allowed    
      ConfigFile(const ConfigFile&);            //Not Allowed    
      ConfigFile& operator=(const ConfigFile&); //Not Allowed

      

    private:
      static std::map<const std::string, ConfigFile*> all_;
 
    protected:
      const std::string name_;
      const std::string directory_;
      QFile file_;
      QFile backupFile_;
    };
  }
}


#endif
