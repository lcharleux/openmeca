// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.



#ifndef OpenMeca_Core_Singleton_hpp
#define OpenMeca_Core_Singleton_hpp

#include <boost/type_traits/is_base_of.hpp>
#include <boost/static_assert.hpp>

#include "OpenMeca/Core/Macro.hpp"

namespace OpenMeca
{
  namespace Core
  {
    
    // The famous singleton design pattern
    template <typename T>
    class Singleton
    {
    protected:
      Singleton();
      virtual ~Singleton();
      
    public:
      static T &Get();  
      static bool Instanciate();

    private:
      static T* me_;
      static unsigned int instanceNumber_;
      
    };
    
    template<class T> T* Singleton<T>::me_ = 0;
    template<class T> unsigned int Singleton<T>::instanceNumber_ = 0;

    template<class T>
    inline
    Singleton<T>::Singleton()
    {
      instanceNumber_++;
      me_ = static_cast<T*>(this);
      OMC_ASSERT_MSG(instanceNumber_ == 1, "The number of singleton instance is superior to 1");
    }

    template<class T>
    inline
    Singleton<T>::~Singleton()
    {
    }
    


    template<class T>
    inline T&
    Singleton<T>::Get()
    {
      BOOST_STATIC_ASSERT_MSG((boost::is_base_of<Singleton<T>, T>::value),
				"T must be a descendant of Singleton");
      if (me_ == 0)
	new T;
      return *me_;
    }
    
    template<class T>
    inline bool
    Singleton<T>::Instanciate()
    {
      if (me_ == 0)
	{
	  new T;
	  return true;
	}
      return false;
    }
    
    
  }
}
#endif
