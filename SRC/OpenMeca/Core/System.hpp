// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_System_hpp
#define OpenMeca_Core_System_hpp

#include <sstream>
#include <vector>


#include "OpenMeca/Core/SetOf.hpp"
#include "OpenMeca/Core/AutoRegister.hpp"
#include "OpenMeca/Core/HistoricManager.hpp"
#include "OpenMeca/Core/UserItem.hpp"
#include "OpenMeca/Util/Version.hpp"
#include "OpenMeca/Util/Var.hpp"
#include "OpenMeca/Setting/Scales.hpp"

#include "ChronoEngine/physics/ChSystem.h"


namespace OpenMeca
{

  namespace Core
  {
    class UserItem;
    class UserRootItem;

    
    // The System class is the openmeca's meastro class. 
    // All the item are registered by the system class.
    // The undo/redo is obtained by serialization/deserialization of the system.
    class System
    {
    public:
      static System& Get();
      static void Quit();
      static bool IsDeleted();
      static const float Version; 

      ~System();
   
      template<class T> SetOf<T>& GetSetOf();
      template<class T> const SetOf<T>& GetSetOf() const;

      bool IsEdited();
      void Update();
      std::string GetAutomaticName(const std::string&) const;
      void SaveFile(const std::string&);
      void LoadFile(const std::string&);
      void Reset();
      HistoricManager& GetHistoric();
      const HistoricManager& GetHistoric() const;
      void Save(std::string&);
      void Load(const std::string&);
      void Draw() const;
      void DrawWithNames() const;

      OpenMeca::Setting::Scales& GetScales();
      const OpenMeca::Setting::Scales& GetScales() const;

      void BuildChSystem();
      void UpdateValueFromCh();
      chrono::ChSystem& GetChSystem();
      
      void SaveState();
      void ResetState();
      void RecoveryState(unsigned int);
      
      void DisableDrawing();
      void EnableDrawing();

      const Util::Version& GetSoftwareVersion() const;

      OMC_ACCESSOR(Time   , double   , time_);

    private :
      System();                          //Not Allowed
      System(const System&);             //Not Allowed
      System& operator=(const System&);  //Not Allowed

      friend class boost::serialization::access;
      template<class Archive> void save(Archive & ar, const unsigned int) const;
      template<class Archive> void load(Archive & ar, const unsigned int);
      BOOST_SERIALIZATION_SPLIT_MEMBER() 
      
      void ManageLoadedFileVersion(const Util::Version& archiveVersion);

    private :
      static System* activeSystem_;
      static bool deleted_;
      static chrono::ChSystem* chSystem_;

    private :
      const std::string version_;
      HistoricManager historic_; //Always at the end !
      bool enableDrawing_;
      double time_;
    }; 
    
    
    

    template<class Archive>
    inline void
    System::save(Archive & ar, const unsigned int version) const
    {
      
      ar << GetSoftwareVersion(); 
      ar << GetSetOf<UserItem>();

      SystemSetting::SaveAll(ar, version);
    }

    template<class Archive>
    inline void
    System::load(Archive & ar, const unsigned int version)
    {
      Util::Version archiveVersion = GetSoftwareVersion();
      if (version == 0)
	{
	  float fileVersion = 0.;
	  ar >> fileVersion; 
	}
      if (version >= 1)
	ar >> archiveVersion; 

      ManageLoadedFileVersion(archiveVersion);

      ar >> GetSetOf<UserItem>();
      SystemSetting::LoadAll(ar, version);
    }

    template<class T>  
    inline SetOf<T>&
    System::GetSetOf()
    {
      return AutoRegister<T>::GetGlobalSet();
    }

    template<class T>  
    inline const SetOf<T>&
    System::GetSetOf() const
    {
      return AutoRegister<T>::GetGlobalSet();
    }

  
  }
}

#include <boost/serialization/version.hpp>
BOOST_CLASS_VERSION(OpenMeca::Core::System, 3)

#endif
