// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Util_Expr_hpp
#define OpenMeca_Util_Expr_hpp


#include <string>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "OpenMeca/Core/Macro.hpp"
#include "OpenMeca/Core/SetOfBase.hpp"
#include "OpenMeca/Util/exprtk.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Util/Unit.hpp"

namespace OpenMeca
{  
  namespace Util
  {

    class Expr
    {
    public:
      static bool IsValid(const std::string &);
      static double Compute(const std::string &);
      static void UpdateAll();

    public:
      Expr();
      Expr(double& var);
      Expr(const std::string & exp, double& var);
      Expr(const Expr&);

      virtual ~Expr();

      void SetDimension(const Util::Dimension& dim);
      const Util::Dimension& GetDimension() const;
      double GetFactor() const;

      void Update();
      double operator()(void);
      double operator()(void) const;
      bool IsNull() const;
      bool IsExpressionNumber() const;

      void SetVariable(double&);
      void SetExpressionFromValue(double);

      bool Check();

      double GetVariableValue() const;

      Expr& operator=(const Expr&);

      const std::string& ToString() const;
      void SetString(std::string);

    private:      
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int);

    private:
      static Core::SetOfBase<Expr> all_;

    protected:
      const Util::Dimension* dim_;
      double* var_;
      std::string expression_;
      bool isCopy_;
    };


    template<class Archive>
    inline void
    Expr::serialize(Archive & ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_NVP(expression_);
    }
    

  }

}




#endif
