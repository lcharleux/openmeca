// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Util_Color_hpp
#define OpenMeca_Util_Color_hpp

#include <QString>
#include <QColor>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>


namespace OpenMeca
{
  namespace Util
  {
    class Color;

    std::ostream& operator<< (std::ostream &, const Color &);

    
    // openmeca's color class
    class Color
    {
    public:
      friend std::ostream& operator<< (std::ostream &, const Color &);

    public:
      static const Color White;
      static const Color Black;
      static const Color Red;
      static const Color Green;
      static const Color Blue;
      static const Color Yellow;

    public:  
      static const QString GetStrType(); 

    public:
      Color();
      Color(unsigned int, unsigned int, unsigned int, unsigned int);    
      Color(unsigned int, unsigned int, unsigned int);    
      Color(const QColor&);
      Color(const Color&);           
      Color& operator=(const Color&);
      ~Color();
      QColor GetQColor() const;
      void ApplyGLColor() const;
      
    private:
      void Randomize();
      int GetRandomNumber();

      friend class boost::serialization::access;
      template<class Archive> void serialize(Archive& ar, const unsigned int version);

    private:
       int red_, green_, blue_, alpha_;
      
    private :
      //Copy constructor and assignment operator are built by compiler
    }; 
    
    template<class Archive>
    inline void
    Color::serialize(Archive& ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_NVP(red_);
      ar & BOOST_SERIALIZATION_NVP(green_);
      ar & BOOST_SERIALIZATION_NVP(blue_);
      ar & BOOST_SERIALIZATION_NVP(alpha_);
    }

  }
}
#endif
