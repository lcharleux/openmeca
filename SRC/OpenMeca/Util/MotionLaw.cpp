// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.




#include "OpenMeca/Util/MotionLaw.hpp"
#include "OpenMeca/Core/System.hpp"


namespace OpenMeca
{  
  namespace Util
  {

    MotionLaw::MotionLaw()
      :enabled_(false),
       law_(Core::System::Get().GetTime(), 0.)
    {
    }

    MotionLaw::~MotionLaw()
    {
    }

    MotionLaw::MotionLaw(const MotionLaw& l):
      enabled_(l.enabled_),
      law_(Core::System::Get().GetTime(), 0.)
    {
      Expr& exp1 = law_;
      const Expr& exp2 = l.law_;
      exp1 = exp2;
    }
    

    MotionLaw& 
    MotionLaw::operator=(const MotionLaw& l)
    {
      enabled_ = l.enabled_;
      Expr& exp1 = law_;
      const Expr& exp2 = l.law_;
      exp1 = exp2;
      return *this;
    }


    bool 
    MotionLaw::IsEnabled() const
    {
      return enabled_;
    }


  }
}
