// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Util_CustomChFunction_hpp
#define OpenMeca_Util_CustomChFunction_hpp

#include "OpenMeca/Util/Expr.hpp"
#include "ChronoEngine/motion_functions/ChFunction_Base.h"


namespace OpenMeca
{  
  namespace Util
  {


    class CustomChFunction : public Expr,
			     public chrono::ChFunction
    {


    public:
      
      CustomChFunction(double&, double);
      virtual ~CustomChFunction();
      
      double Get_y      (double);
      double Get_y_dx   (double);
      double Get_y_dxdx (double);

      virtual ChFunction* new_Duplicate ();

    private:
      void PostSerializationLoad();

      friend class boost::serialization::access;
      template<class Archive> void save(Archive& ar, const unsigned int) const;
      template<class Archive> void load(Archive& ar, const unsigned int);
      BOOST_SERIALIZATION_SPLIT_MEMBER()

    private:
      double& x_;
      double  y_;
    };

    template<class Archive>
    inline void
    CustomChFunction::save(Archive& ar, const unsigned int) const
    {
      ar << BOOST_SERIALIZATION_BASE_OBJECT_NVP(Expr);
      ar << BOOST_SERIALIZATION_NVP(y_);
    }

    template<class Archive>
    inline void
    CustomChFunction::load(Archive& ar, const unsigned int version)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Expr);
      if (version >= 1)
	{
	  ar & BOOST_SERIALIZATION_NVP(y_);
	  PostSerializationLoad();
	}
    }

  }
}

#include <boost/serialization/version.hpp>
BOOST_CLASS_VERSION(OpenMeca::Util::CustomChFunction, 1)

#endif
