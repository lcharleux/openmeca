// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Item_ExprAttitude_hpp
#define OpenMeca_Item_ExprAttitude_hpp


#include <string>


#include "OpenMeca/Core/Macro.hpp"
#include "OpenMeca/Util/Expr.hpp"
#include "OpenMeca/Geom/Quaternion.hpp"
#include "OpenMeca/Util/ExprVector.hpp"
#include "OpenMeca/Util/ExprDouble.hpp"


namespace OpenMeca
{  
  namespace Util
  {

    class ExprAttitude : public Geom::Quaternion<_3D>
    {

    public:
      ExprAttitude(std::function<const Geom::Frame<_3D>& ()> = &Geom::Frame<_3D>::GetGlobal);
      virtual ~ExprAttitude();

      ExprAttitude& operator=(const Geom::Quaternion<_3D>&); 
      ExprAttitude& operator=(const ExprAttitude&); 
      void Update();

      OMC_ACCESSOR(Axis , ExprVector, axis_ );
      OMC_ACCESSOR(Angle, ExprDouble, angle_);

    private:      
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int);

    private:
      ExprVector axis_;
      ExprDouble angle_;
    };


    template<class Archive>
    inline void
    ExprAttitude::serialize(Archive & ar, const unsigned int)
    {
      ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Geom::Quaternion<_3D>);
      ar & BOOST_SERIALIZATION_NVP(axis_);
      ar & BOOST_SERIALIZATION_NVP(angle_);
    }
    

  }

}




#endif
