// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <QFile>

#ifndef OSX
  #include <GL/gl.h>
#else
  #include <OpenGL/gl.h>
#endif

#include "OpenMeca/Util/Color.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"

namespace OpenMeca
{
  namespace Util
  {
    const Color Color::White  = Color(Qt::white);
    const Color Color::Black  = Color(Qt::black);
    const Color Color::Red    = Color(Qt::red);
    const Color Color::Green  = Color(Qt::green);
    const Color Color::Blue   = Color(Qt::blue);
    const Color Color::Yellow = Color(Qt::yellow);


    std::ostream& 
    operator<< (std::ostream& os, const Color & c)
    {
      os << "red: "   << c.red_ << '\t'
	 << "green: " << c.green_ << '\t'
	 << "blue: "  << c.blue_ << '\t'
	 << "alpha: " << c.alpha_;
      return os;
    }


    const QString
    Color::GetStrType()
    {
      return "Color";
    }

    Color::Color()
      :red_(0),
       green_(0),
       blue_(0),
       alpha_(255)
       
    {
      srand ( time(NULL) );
      Randomize();
    }

    Color::Color(unsigned int r, unsigned int g, unsigned int b, unsigned int a)
      :red_(r),
       green_(g),
       blue_(b),
       alpha_(a)
       
    {
    }

    Color::Color(unsigned int r, unsigned int g, unsigned int b)
      :red_(r),
       green_(g),
       blue_(b),
       alpha_(255)
       
    {
    }

    Color::Color(const QColor& col)
      :red_(col.red()),
       green_(col.green()),
       blue_(col.blue()),
       alpha_(col.alpha())
       
    {
    }
    
    Color::Color(const Color& col)
      :red_(col.red_),
       green_(col.green_),
       blue_(col.blue_),
       alpha_(col.alpha_)
    {
    }


   
    Color::~Color()
    {
    }

    Color&  
    Color::operator=(const Color& col)
    {
      red_ = col.red_;
      green_ = col.green_;
      blue_ = col.blue_;
      alpha_ = col.alpha_;
      return *this;
    }

    void
    Color::Randomize()
    {
      red_   = GetRandomNumber();
      green_ = GetRandomNumber();
      blue_  = GetRandomNumber();
    }

    int
    Color::GetRandomNumber()
    {
      int val = rand() % 256;
      OMC_ASSERT_MSG( val >= 0 && val <= 255, "Wrong values");
      return val;
    }

    QColor
    Color:: GetQColor() const
    {
      return QColor(red_, green_, blue_, alpha_);
    }
   
    void
    Color::ApplyGLColor() const
    {
      float red   = float(red_)  /255.;
      float green = float(green_)/255.;
      float blue  = float(blue_) /255.;
      float alpha = float(alpha_)/255.;
      glColor4f(red, green, blue, alpha);
    }
    

  }
}
