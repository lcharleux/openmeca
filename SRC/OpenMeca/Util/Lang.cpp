// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Util/Lang.hpp"
#include "OpenMeca/Util/Dimension.hpp"


namespace OpenMeca
{
  namespace Util
  {

    std::string Lang::GetStrType(){return "Lang";}

    std::map<const std::string, Lang*> Lang::all_ = std::map<const std::string, Lang*>();

    Lang& 
    Lang::GetByID(const std::string& id)
    {
      OMC_ASSERT_MSG(all_.count(id) == 1, "Can't find this id");
      return *all_[id];
    }

    Lang& 
    Lang::GetByIndex(const int index)
    {
      int i = 0;
      std::map<const std::string, Lang*>::iterator it;
      for (it = all_.begin(); it != all_.end(); ++it)
	{
	  if (i == index)
	    return *it->second;
	  i ++;
	}
      OMC_ASSERT_MSG(0, "Can't find this index");
      return *it->second;
    }

    std::map<const std::string, Lang*>& 
    Lang::GetAll()
    {
      return all_;
    }


    Lang::Lang(const std::string& id, const std::string& name, const std::string& file)
      :id_(id),
       name_(name), 
       file_(file)
    {
      OMC_ASSERT_MSG(all_.count(id_) == 0, "The id is already taken");
      all_[id_] = this;
    }
    

    Lang::~Lang()
    {
      OMC_ASSERT_MSG(all_.count(id_) == 1, "Can't find this id");
      all_.erase(id_); 
    }


    int
    Lang::GetIndex() const
    {
      int i = 0;
      std::map<const std::string, Lang*>::iterator it;
      for (it = all_.begin(); it != all_.end(); ++it)
	{
	  if (it->second == this)
	    return i;
	  i ++;
	}
      OMC_ASSERT_MSG(0, "Can't find this index");
      return i;
    }
      
  }
}
