// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QThread>

#include "OpenMeca/Setting/Simulation.hpp"
#include "OpenMeca/Core/SystemSettingCommonProperty.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Gui/MainWindow.hpp"
#include "OpenMeca/Gui/Dialog/DialogSimulation.hpp"
#include "OpenMeca/Item/Sensor.hpp"

#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Setting::Simulation)
BOOST_CLASS_EXPORT(OpenMeca::Core::SystemSettingT<OpenMeca::Setting::Simulation>)


namespace OpenMeca
{
  namespace Setting
  {
    
    const std::string 
    Simulation::GetStrType()
    {
      return "Simulation";
    }

    const QString 
    Simulation::GetQStrType()
    {
      return QObject::tr("Simulation");
    }

    void
    Simulation::Init()
    {
      Core::SystemSettingT< Simulation>::Get();      
      Core::Singleton< Core::SystemSettingCommonProperty<Simulation> >::Get().RegisterAction();
    }



    Simulation::Simulation()
      :QThread(),
       Core::SystemSettingT<Simulation>(),
       initialize_(false),
       current_(),
       recordTime_(),
       timeArray_(),
       timeStep_(0.1),
       totalSimulationTime_(60.),
      solver_(chrono::ChSystem::LCP_ITERATIVE_SOR),
      integrationType_(chrono::ChSystem::INT_ANITESCU),
       stopLoopRequired_(false),
       pauseLoopRequired_(false),
       animationPeriod_(.1),
      isRunning_(false),
      staticEquilibrium_(false)
    {
      
    }

    Simulation::~Simulation()
    {
    }

     void
    Simulation::Start()
    {
      isRunning_ = true;
      if (!initialize_)
	Initialize();

      stopLoopRequired_ = false;
      pauseLoopRequired_ = false;
      Gui::MainWindow::Get().GetViewer().setAnimationPeriod(int(animationPeriod_*1000.));
      Gui::MainWindow::Get().GetViewer().startAnimation();
      QThread::start();
    }

    
    void
    Simulation::run()
    {
      do
	{
	  NextStep();
	}
      while(pauseLoopRequired_ == false);

      if (stopLoopRequired_ == true)
	{
	  OMC_ASSERT_MSG(Gui::MainWindow::Get().GetViewer().animationIsStarted() == false,
			 "The animation is already started");
	  Initialize();
	}
    }


    void
    Simulation::Pause()
    {
      Gui::MainWindow::Get().GetViewer().stopAnimation();
      pauseLoopRequired_ = true;
    }

    void
    Simulation::Stop()
    {
      stopLoopRequired_ = true;
      Pause();
      wait();
      
      Core::System::Get().ResetState();
      Gui::MainWindow::Get().GetViewer().updateGL();
      Initialize();
      stopLoopRequired_ = false;
      initialize_ = false;
      pauseLoopRequired_ = true;
      isRunning_ = false;
    }
    
    void
    Simulation::NextStep()
    {
      if (current_.time >= totalSimulationTime_)
	{
	  emit End();
	  return;
	}

      if (current_.iterNumber < current_.totalIterNumber)
	current_.iterNumber  = current_.totalIterNumber;

      if (staticEquilibrium_ == false)
	Core::System::Get().GetChSystem().DoStepDynamics(timeStep_);
      else
	Core::System::Get().GetChSystem().DoStaticRelaxing();

      OMC_ASSERT_MSG(recordTime_.count(current_.iterNumber) == 1, "the data are not coherent");
      current_.time = recordTime_[current_.iterNumber];
      current_.time += timeStep_;
      current_.iterNumber ++;
      current_.totalIterNumber = current_.iterNumber;
      recordTime_[current_.iterNumber] = current_.time;
      timeArray_.push_back(current_.time);
      Core::System::Get().UpdateValueFromCh();
      Core::System::Get().SaveState();
      emit StepChanged(current_);

    }

    void
    Simulation::Initialize()
    {
      Core::System::Get().ResetState();
      Core::System::Get().BuildChSystem();
      initialize_ = true;
      pauseLoopRequired_ = false;
      stopLoopRequired_ = false;
      recordTime_.clear();
      timeArray_.clear();
      current_.Init();
      recordTime_[current_.iterNumber] = current_.time;
      timeArray_.push_back(current_.time);
      Core::System::Get().SaveState();
      emit StepChanged(current_);
    }

    void
    Simulation::InitChSystem(chrono::ChSystem& ch_system)
    {
      ch_system.SetLcpSolverType(GetSolver());
      ch_system.SetIntegrationType(GetIntegrationType());
    }
  
    void
    Simulation::BuildChSystem(chrono::ChSystem&)
    {
    }

    void
    Simulation::GoToStep(unsigned int iter)
    {
      Pause();
      OMC_ASSERT_MSG(recordTime_.count(iter) == 1,
		 "The data are not coherent");
      current_.time = recordTime_[iter];
      current_.iterNumber = iter;
      Core::System::Get().RecoveryState(iter);
      Gui::MainWindow::Get().GetViewer().updateGL();
      emit StepChanged(current_);
    }
   
    double& 
    Simulation::GetTimeStep()
    {
      return timeStep_;
    }

    const double& 
    Simulation::GetTimeStep() const
    {
      return timeStep_;
    }
    
    double& 
    Simulation::GetTotalSimulationTime()
    {
      return totalSimulationTime_;
    }
    
    const double& 
    Simulation::GetTotalSimulationTime() const
    {
      return totalSimulationTime_;
    }
    
    chrono::ChSystem::eCh_lcpSolver& 
    Simulation::GetSolver()
    {
      return solver_;
    }
    
    const chrono::ChSystem::eCh_lcpSolver& 
    Simulation::GetSolver() const
    {
      return solver_;
    }

    chrono::ChSystem::eCh_integrationType& 
    Simulation::GetIntegrationType()
    {
      return integrationType_;
    }


    const chrono::ChSystem::eCh_integrationType& 
    Simulation::GetIntegrationType() const
    {
      return integrationType_;
    }

    const std::vector<double>& 
    Simulation::GetTimeArray()
    {
      return timeArray_;
    }


    bool
    Simulation::IsRunning() const
    {
      return isRunning_;
    }
    
    void
    Simulation::Save(boost::archive::text_oarchive& ar, const unsigned int )
    {
      ar << BOOST_SERIALIZATION_NVP(timeStep_);
      ar << BOOST_SERIALIZATION_NVP(totalSimulationTime_);
      ar << BOOST_SERIALIZATION_NVP(animationPeriod_);
      ar << BOOST_SERIALIZATION_NVP(solver_);
      ar << BOOST_SERIALIZATION_NVP(integrationType_);
      ar << BOOST_SERIALIZATION_NVP(staticEquilibrium_);
    }

    void
    Simulation::Load(boost::archive::text_iarchive& ar, const unsigned int version)
    {
      ar >> BOOST_SERIALIZATION_NVP(timeStep_);
      ar >> BOOST_SERIALIZATION_NVP(totalSimulationTime_);
      if (version >= 2)
	ar >> BOOST_SERIALIZATION_NVP(animationPeriod_);
      if (version >= 3)
	{
	  ar >> BOOST_SERIALIZATION_NVP(solver_);
	  ar >> BOOST_SERIALIZATION_NVP(integrationType_);
	  ar >> BOOST_SERIALIZATION_NVP(staticEquilibrium_);
	}
    }

  }
}
 

