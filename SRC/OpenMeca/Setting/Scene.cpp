// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Setting/Scene.hpp"
#include "OpenMeca/Item/Part.hpp"
#include "OpenMeca/Core/System.hpp"
#include "OpenMeca/Core/SystemSettingCommonProperty.hpp"
#include "OpenMeca/Gui/Dialog/DialogScene.hpp"


namespace OpenMeca
{
  namespace Setting
  {

    const 
    std::string Scene::GetStrType()
    {
      return "Scene";
    }

    const 
    QString Scene::GetQStrType()
    {
      return QObject::tr("Scene");
    }

    void
    Scene::Init()
    {
      Core::SystemSettingT<Scene>::Get(); 
      Core::Singleton< Core::SystemSettingCommonProperty<Scene> >::Get().RegisterAction();
      
    }


    Scene::Scene()
      :Core::SystemSettingT<Scene>(),
       drawAxis_(true),
       backgroundColor_(Qt::darkGray),
       sceneCenter_(0.,0.,0.),
       sceneRadius_(1.),
       cameraType_(qglviewer::Camera::PERSPECTIVE)
    {
    }

    Scene::~Scene()
    {
    }

    void
    Scene::Update()
    {
      Gui::Viewer& viewer = Gui::MainWindow::Get().GetViewer();
      viewer.setBackgroundColor(backgroundColor_.GetQColor());
      viewer.setSceneCenter(sceneCenter_.ToQGLVector());
      viewer.setSceneRadius(sceneRadius_);
      viewer.camera()->setType(cameraType_);
      viewer.DrawAxis(drawAxis_);
      viewer.updateGL();
    }

    
    void
    Scene::ComputeOptimalSceneRadius()
    {
      double rmax = 0;

      Core::SetOf<OpenMeca::Item::Part>::it it;
      Core::SetOf<OpenMeca::Item::Part>& set = 
	Core::System::Get().GetSetOf<OpenMeca::Item::Part>();

      for (it = set.Begin() ; it != set.End(); it++ )
    	{
    	  const Geom::Point<_3D>& p = (*it)->GetFrame().GetCenter();
    	  const Geom::Point<_3D> pp(p, &Geom::Frame<_3D>::GetGlobal);
    	  const double norm = pp.GetPositionVector().GetNorm();
    	  if (norm > rmax)
    	    rmax = norm;
    	  (*it)->Update();
    	}
      sceneRadius_ = rmax;
    }


    void 
    Scene::Save(boost::archive::text_oarchive& ar, const unsigned int )
    {
      ar << BOOST_SERIALIZATION_NVP(drawAxis_);
      ar << BOOST_SERIALIZATION_NVP(backgroundColor_);
      ar << BOOST_SERIALIZATION_NVP(sceneCenter_);
      ar << BOOST_SERIALIZATION_NVP(sceneRadius_);
      ar << BOOST_SERIALIZATION_NVP(cameraType_);

    }

    void 
    Scene::Load(boost::archive::text_iarchive& ar, const unsigned int ) 
    {
      ar >> BOOST_SERIALIZATION_NVP(drawAxis_);
      ar >> BOOST_SERIALIZATION_NVP(backgroundColor_);
      ar >> BOOST_SERIALIZATION_NVP(sceneCenter_);
      ar >> BOOST_SERIALIZATION_NVP(sceneRadius_);
      ar >> BOOST_SERIALIZATION_NVP(cameraType_);
    }


  }
}
 

