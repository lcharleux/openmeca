// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef OpenMeca_Core_MenuManager_hpp
#define OpenMeca_Core_MenuManager_hpp

#include <QDomDocument>
#include <QMenu>

#include "OpenMeca/Core/GlobalSettingT.hpp"
#include "OpenMeca/Core/System.hpp"


namespace OpenMeca
{
  namespace Setting
  {

    // The MenuManager manage the file that customize the openmeca's menus
    class MenuManager : public Core::GlobalSettingT<MenuManager>  
    {
      friend class Core::Singleton<MenuManager> ;

    public:  
      static void Init();
      static const std::string GetStrType(); 
      typedef double Dialog; // fake, change this plz !
  
      std::string GetClassId() const;
      void ReadXmlFile();
      void WriteXmlFile();
      QAction* GetQAction(const std::string& actionID);

    private:
      MenuManager();
      ~MenuManager();
      void ReadMenu(QDomElement& e, QMenu* menu);
      void AddAction(QDomElement& e, QMenu* menu);
      QIcon BuildIcon(QDomElement& e);

    private:
      const std::string menuKey_;
      const std::string classKey_;
      const std::string actionClassKey_;
      const std::string actionKey_;
      const std::string separatorKey_;
      std::map<const std::string, QAction*> actions_;
      
    private :
      MenuManager(const MenuManager&);             //Not Allowed
      MenuManager& operator=(const MenuManager&);  //Not Allowed
    }; 
    
    

  }
}
#endif
