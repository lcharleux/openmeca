// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QWidget>

#include "OpenMeca/Physic/AngularPosition.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Core/System.hpp"

#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::AngularPosition)

namespace OpenMeca
{  
  namespace Physic
  {

    const Util::Color AngularPosition::color = Util::Color::Green;
    
    const std::string
    AngularPosition::GetStrType()
    {
      return "AngularPosition";
    }

    const QString
    AngularPosition::GetQStrType()
    {
      return QObject::tr("AngularPosition");
    }
      

    AngularPosition::AngularPosition(Item::Physical& item)
      :QuantityT<Double>(item, color),
       unit_(Util::Dimension::Get("Angle").GetUnit("Radian"))
    {
      
    }
   
    AngularPosition::~AngularPosition()
    {
    }

 
    const Util::Unit& 
    AngularPosition::GetUnit() const
    {
      return unit_;
    }
    
    double
    AngularPosition::GetScale() const
    {
      OMC_ASSERT_MSG(0, "Can't call this method");
      return Core::System::Get().GetScales().GetScaleValue(AngularPosition::GetStrType());
    }



  }
}
