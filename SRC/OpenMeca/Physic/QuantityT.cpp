// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.




#include "OpenMeca/Physic/QuantityT.hpp"
#include "OpenMeca/Physic/Vector3D.hpp"

#include "OpenMeca/Item/Physical.hpp"

#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::QuantityT<OpenMeca::Physic::Vector3D>)


namespace OpenMeca
{
  namespace Physic
  {    

    template<>
    void 
    QuantityT<Vector3D>::Draw()
    {
      if (data_.GetRealType().GetFrame() == GetPhysicalItem().GetFrame())
	data_.Draw();
      else
	OMC_ASSERT_MSG(data_.GetRealType().GetFrame() == Geom::Frame<_3D>::Global,
		       "The vector must be expression in global frame");
    }

    template<>
    void 
    QuantityT<Vector3D>::BeginDraw()
    {
      // if the vector is expressed in the global frame use begin draw
      if (data_.GetRealType().GetFrame() == Geom::Frame<_3D>::Global)
	{
	  const Geom::Point<_3D>& p_loc  = GetPhysicalItem().GetFrame().GetCenter();
          const Geom::Point<_3D> p(p_loc, Geom::Frame<_3D>::GetGlobal);
	  glTranslatef(p[0], p[1], p[2]);
	  data_.Draw();
          glTranslatef(-p[0], -p[1], -p[2]);
	}
    }

    template<>
    void 
    QuantityT<Vector3D>::EndDraw()
    {
      Quantity::EndDraw();
    }

  }
}


