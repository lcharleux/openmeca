// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QWidget>

#include "OpenMeca/Physic/LinearPosition.hpp"
#include "OpenMeca/Util/Dimension.hpp"
#include "OpenMeca/Core/System.hpp"

#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::LinearPosition)

namespace OpenMeca
{  
  namespace Physic
  {

    const Util::Color LinearPosition::color = Util::Color::Blue;
    
    const std::string
    LinearPosition::GetStrType()
    {
      return "LinearPosition";
    }

    const QString
    LinearPosition::GetQStrType()
    {
      return QObject::tr("LinearPosition");
    }
      

    LinearPosition::LinearPosition(Item::Physical& item)
      :QuantityT<Vector3D>(item, color),
       unit_(Util::Dimension::Get("Length").GetUnit("Meter"))
    {
      
    }
   
    LinearPosition::~LinearPosition()
    {
    }

 
    const Util::Unit& 
    LinearPosition::GetUnit() const
    {
      return unit_;
    }
    
    double
    LinearPosition::GetScale() const
    {
      return Core::System::Get().GetScales().GetScaleValue(LinearPosition::GetStrType());
    }

    void
    LinearPosition::BeginDraw()
    {
      const std::vector< std::array<double, 3> >& array = 
	GetRealDataType().GetRealType().GetRecordedState();
	
	if (array.size() > 2)
	  {
	    glDisable(GL_LIGHTING);
	    glLineWidth(1);
	    glColor4f(1.0, 1.0, 1.0, 1.0);
	    glBegin(GL_LINE_STRIP);
	    for (unsigned int i = 0; i < array.size(); ++i)
	      glVertex3f(array[i][0], array[i][1], array[i][2]);

	    glEnd();
	    glEnable(GL_LIGHTING);
	  }
    }

    void
    LinearPosition::Draw()
    {
    }

  }
}
