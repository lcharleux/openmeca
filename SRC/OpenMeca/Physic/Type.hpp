// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#ifndef _OpenMeca_Physic_Type_hpp_
#define _OpenMeca_Physic_Type_hpp_

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/array.hpp>

#include <QIcon>
#include <QTreeWidget>
#include "OpenMeca/Util/Unit.hpp"
#include "OpenMeca/Geom/Frame.hpp"

namespace OpenMeca
{
  namespace Physic
  {

    class Quantity;

    class Type
    {
    public:
      Type(Quantity&);
      virtual ~Type();

      const std::string& GetLabel() const;
      double GetScale() const;
      const Util::Unit& GetUnit() const;

      Quantity& GetQuantity();
      const Quantity& GetQuantity() const;

      virtual void SaveState() = 0;
      virtual void ResetState() = 0;
      virtual void RecoveryState(unsigned int) = 0;
      virtual void FillDataTree(QTreeWidgetItem* item) = 0;
      virtual void UpdateDataTree() = 0;
      virtual void WriteHeaderDataFile(std::ofstream&) = 0;
      virtual void WriteDataFile(std::ofstream&, unsigned int) = 0;

      virtual void Draw() const = 0;

    protected:
      QTreeWidgetItem* dataTreeItem_;

      
    private:
      friend class boost::serialization::access;
      template<class Archive>
      void serialize(Archive & ar, const unsigned int);

    private:
      Quantity& quantity_;

    protected:
      const QIcon plusIcon_;
    };

    template<class Archive>
    inline void
    Type::serialize(Archive &, const unsigned int)
    {
    }


  }
} 




#endif
