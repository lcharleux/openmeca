// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include "OpenMeca/Physic/Vector3D.hpp"
#include "OpenMeca/Gui/MainPlotWindow.hpp"

#include <QPushButton>

#include <functional>
#include <boost/bind.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/array.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::Vector3D)

namespace OpenMeca
{
  namespace Physic
  {

    Vector3D::Vector3D(Quantity& q)
      :TypeT<Geom::Vector<_3D> >(q),
      vec_(boost::bind(&Quantity::GetFrame, boost::ref(q))), 
      x_(), y_(), z_(), norm_(),
      xfunc_(0), yfunc_(0), zfunc_(0),
      xitem_(0), yitem_(0), zitem_(0), nitem_(0)
    {
      const Geom::Frame<_3D>&(Quantity::*fnpt)() const= &Quantity::GetFrame;
      std::function<const Geom::Frame<_3D>& ()> f_body = boost::bind(fnpt, boost::ref(q));
      

    }

    void
    Vector3D::SetCustomChFunction(Util::CustomChFunction* x, Util::CustomChFunction* y, Util::CustomChFunction* z)
    {
      xfunc_ = x;
      yfunc_ = y;
      zfunc_ = z;
    }

    
    Vector3D::~Vector3D()
    {
    }

    Geom::Vector<_3D>& 
    Vector3D::GetRealType()
    {
      return vec_;
    }
    
    const Geom::Vector<_3D>& 
    Vector3D::GetRealType() const
    {
      return vec_;
    }
    
    void
    Vector3D::SaveState()
    {
      if (xfunc_ != 0 && yfunc_ != 0 && zfunc_ != 0)
	{
	  vec_[0] = (*xfunc_)();
	  vec_[1] = (*yfunc_)();
	  vec_[2] = (*zfunc_)();
	}
	
      Update(vec_);
    }

    void Vector3D::UpdateValueWithChFunction()
    {
      if (xfunc_ != 0 && yfunc_ != 0 && zfunc_ != 0)
	{
	  vec_[0] = (*xfunc_)();
	  vec_[1] = (*yfunc_)();
	  vec_[2] = (*zfunc_)();
	}
    }

    void
    Vector3D::Update()
    {
      x_.push_back(0);
      y_.push_back(0);
      z_.push_back(0);
      norm_.push_back(0);
      vec_.SaveState();
    }


    void
    Vector3D::Update(const Geom::Vector<_3D>& v)
    {
      vec_ = v;
      x_.push_back(v[0]);
      y_.push_back(v[1]);
      z_.push_back(v[2]);
      norm_.push_back(v.GetNorm());
      vec_.SaveState();
    }

    void
    Vector3D::ResetState()
    {
      vec_.ResetState();
      x_.clear();
      y_.clear();
      z_.clear();
      norm_.clear();
    }

    void
    Vector3D::RecoveryState(unsigned int i)
    {
      vec_.RecoveryState(i);
    }

    void 
    Vector3D::FillDataTree(QTreeWidgetItem* tree)
    {
      dataTreeItem_ = tree;

      xitem_ =  new QTreeWidgetItem(tree);
      yitem_ =  new QTreeWidgetItem(tree);
      zitem_ =  new QTreeWidgetItem(tree);
      nitem_ =  new QTreeWidgetItem(tree);

      xitem_->setText(0, "X");
      yitem_->setText(0, "Y");
      zitem_->setText(0, "Z");
      nitem_->setText(0, "Norm");
      UpdateDataTree();

      auto xbutton = new QPushButton(plusIcon_, "");
      xbutton->setCheckable(true);
      xbutton->setMaximumSize(20, xbutton->height());
      tree->treeWidget()->setItemWidget(xitem_, 2, xbutton);
      
      auto ybutton = new QPushButton(plusIcon_, "");
      ybutton->setCheckable(true);
      ybutton->setMaximumSize(20, ybutton->height());
      tree->treeWidget()->setItemWidget(yitem_, 2, ybutton);

      auto zbutton = new QPushButton(plusIcon_, "");
      zbutton->setCheckable(true);
      zbutton->setMaximumSize(20, zbutton->height());
      tree->treeWidget()->setItemWidget(zitem_, 2, zbutton);

      auto nbutton = new QPushButton(plusIcon_, "");
      nbutton->setCheckable(true);
      nbutton->setMaximumSize(20, nbutton->height());
      tree->treeWidget()->setItemWidget(nitem_, 2, nbutton);
      
      QObject::connect(xbutton, SIGNAL(toggled(bool)), this, SLOT(X(bool)));
      QObject::connect(ybutton, SIGNAL(toggled(bool)), this, SLOT(Y(bool)));
      QObject::connect(zbutton, SIGNAL(toggled(bool)), this, SLOT(Z(bool)));
      QObject::connect(nbutton, SIGNAL(toggled(bool)), this, SLOT(Norm(bool)));

      tree->setExpanded(true);
    }

    void 
    Vector3D::UpdateDataTree()
    {
      OMC_ASSERT_MSG(xitem_!=0, "DataTreeItem has not been instantiate !");
      OMC_ASSERT_MSG(yitem_!=0, "DataTreeItem has not been instantiate !");
      OMC_ASSERT_MSG(zitem_!=0, "DataTreeItem has not been instantiate !");
      
      const QString unit = QString(" ") + GetUnit().GetSymbol().c_str();
      if (x_.size() > 0)
	xitem_->setText(1,QString::number(x_.back()) + unit);
      if (y_.size() > 0)
	yitem_->setText(1,QString::number(y_.back()) + unit);
      if (z_.size() > 0)
	zitem_->setText(1,QString::number(z_.back()) + unit);
      if (norm_.size() > 0)
	nitem_->setText(1,QString::number(norm_.back()) + unit);
    }
      
    void 
    Vector3D::X(bool val)
    {
      if (val)
	{
	  std::string label = GetLabel();
	  label += " along X";
	  Gui::MainPlotWindow::Get().AddData(x_, label.c_str(), GetUnit());
	}
      else
	Gui::MainPlotWindow::Get().RemoveData(x_);
    }

    void 
    Vector3D::Y(bool val)
    {
      if (val)
	{
	  std::string label =  GetLabel();
	  label += " along Y";
	  Gui::MainPlotWindow::Get().AddData(y_, label.c_str(), GetUnit());
	}
      else
	Gui::MainPlotWindow::Get().RemoveData(y_);
    }

    void 
    Vector3D::Z(bool val)
    {
      if (val)
	{
	  std::string label =  GetLabel();
	  label += " along Z";
	  Gui::MainPlotWindow::Get().AddData(z_, label.c_str(), GetUnit());
	}
      else
	Gui::MainPlotWindow::Get().RemoveData(z_);
    }

    void 
    Vector3D::Norm(bool val)
    {
      if (val)
	{
	  std::string label =  GetLabel();
	  label = "Norm of " + label;
	  Gui::MainPlotWindow::Get().AddData(norm_, label.c_str(), GetUnit());
	}
      else
	Gui::MainPlotWindow::Get().RemoveData(norm_);
    }

    void 
    Vector3D::WriteHeaderDataFile(std::ofstream& file)
    {
      file << GetLabel() << " /X (" <<  GetUnit().GetSymbol() << ")\t"
	   << GetLabel() << " /Y (" <<  GetUnit().GetSymbol() << ")\t"
	   << GetLabel() << " /Z (" <<  GetUnit().GetSymbol() << ")";
    }

    void 
    Vector3D::WriteDataFile(std::ofstream& file, unsigned int iter)
    {
      OMC_ASSERT_MSG(iter < x_.size(), "The data are not coherent");
      OMC_ASSERT_MSG(iter < y_.size(), "The data are not coherent");
      OMC_ASSERT_MSG(iter < z_.size(), "The data are not coherent");
      file << x_[iter] << '\t'
	   << y_[iter] << '\t'
	   << z_[iter];
    }

    void 
    Vector3D::Draw() const
    {
      vec_.Draw(GetScale());
    }


  

  }
} 

