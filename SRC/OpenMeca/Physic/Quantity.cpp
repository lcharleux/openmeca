// This file is part of OpenMeca, an easy software to do mechanical simulation.
//
// Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
//
// Copyright (C) 2012 Damien ANDRE
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QWidget>

#include "OpenMeca/Physic/Quantity.hpp"
#include "OpenMeca/Physic/Type.hpp"
#include "OpenMeca/Item/Physical.hpp"

#include <boost/serialization/export.hpp>
//Don't forget to export for dynamic serialization of child class
BOOST_CLASS_EXPORT(OpenMeca::Physic::Quantity)

namespace OpenMeca
{
  namespace Physic
  {

    Quantity::Quantity(Item::Physical& item, const Util::Color& color)
      :item_(item),
       color_(color)
    {
    }
    
    Quantity::~Quantity()
    {
    }
    
    void
    Quantity::Draw()
    {
      GetDataType().Draw();
    }

    void
    Quantity::BeginDraw()
    {
    }

    void
    Quantity::EndDraw()
    {
    }

    const Geom::Frame<_3D>& 
    Quantity::GetFrame() const
    {
      return Geom::Frame<_3D>::Global;
    }

    const std::string& 
    Quantity::GetLabel()
    {
      return item_.GetName();
    }

    const Util::Color& 
    Quantity::GetColor() const
    {
      return color_;
    }
      
    Util::Color& 
    Quantity::GetColor()
    {
      return color_;
    }
      
    const Item::Physical& 
    Quantity::GetPhysicalItem() const
    {
      return item_;
    }

    Item::Physical& 
    Quantity::GetPhysicalItem()
    {
      return item_;
    }

     void
     Quantity::Init()
     {
     }

  }
}
