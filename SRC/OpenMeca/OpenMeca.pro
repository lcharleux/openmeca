## This file is part of OpenMeca, an easy software to do mechanical simulation.
##
## Author(s)    :  - Damien ANDRE  <openmeca@gmail.com>
##
## Copyright (C) 2012 Damien ANDRE
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.



TEMPLATE  = app
TARGET    = ./BUILD/openmeca
DESTDIR   = ./
CONFIG   += qt silent qwt


QMAKE_CXXFLAGS += -Wno-unused-local-typedefs

QT +=  help svg xml opengl sql

TRANSLATIONS = ./Rsc/Lang/openmeca_fr.ts

MOC_DIR     = ./BUILD/moc
UI_DIR      = ./BUILD/ui
OBJECTS_DIR = ./BUILD/obj
RCC_DIR     = ./BUILD/rcc


DEPENDPATH += ../../
 
INCLUDEPATH = ./ ../../ ../ \
              ./BUILD/ui \
              ../ChronoEngine/ \
              ../ChronoEngine/collision/bullet \
              ../ChronoEngine/collision/gimpact \
               ../../../LIB/QGLViewer/BUILD/include \
              /usr/include/qwt

LIBS      +=  ../ChronoEngine/BUILD/lib/libchronoengine.a 


 
win32 {
LIBS   += -L$$(mxe)/usr/i686-w64-mingw32.static/lib -L$$(mxe)/usr/i686-w64-mingw32.static/qt5/lib -lboost_serialization-mt -lqwt ../../../LIB/QGLViewer/BUILD/libQGLViewer.a -lglu32
CONFIG += release C++11
}

unix:!macx {
LIBS   += -lGL -lGLU -lboost_serialization -lqwt-qt5 -lQGLViewer-qt5
CONFIG += release
}

macx {
QMAKE_CXXFLAGS += -DOSX
QMAKE_CXXFLAGS += -ftemplate-depth=1024
CONFIG         += release
ICON            = ./Rsc/Img/OpenMeca.icns
TARGET          = ./BUILD/OpenMeca
LIBS           += -lGL -lGLU -lboost_serialization -lqwt-qt5
}

RESOURCES += ./Resources.qrc
SOURCES += Main.cpp

include(./Core/Core.pro)
include(./Physic/Physic.pro)
include(./Util/Util.pro)
include(./Geom/Geom.pro)
include(./Gui/Gui.pro)
include(./Item/Item.pro)
include(./Setting/Setting.pro)
