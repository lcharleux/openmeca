

## OpenMeca is a multibody dynamics software designed for human being. ##

OpenMeca is based on the [chronoengine](http://chronoengine.info/chronoengine/) library.
The aim of openmeca is to provide a software for simulating mechanical systems easily.
openmeca allow us to builds a 3D sketch, where the bonds are represented by symbols and gives a simple way 
to apply loading and boundary conditions. Thanks to numerical sensors,
different kind of data (force, torque, displacement, velocity, etc.) 
could be extracted from the simulation. 

# Example video
[![Screenshot](http://www.yakuru.fr/~openmeca/yt-screenshot.png)](http://www.youtube.com/watch?v=8iGKT1DO9Jc "Screen cast of epicyclic gearing")


# Usage
A tutorial is available on YouTube [here](https://www.youtube.com/playlist?list=PLjFD44HqZ1diZN45FSLO82NyuIgpu4Lml).

# Installation on Mac OS x
Simply download the installer [here](http://www.yakuru.fr/~openmeca/openmeca.dmg). 
To install it, if you are not familiar with .dmg files:
 - double click on the downloaded .dmg file
 - in the window that just appeared, drag and drop the OpenMeca onto the link to Applications folder.

To launch OpenMeca, navigate to the Applications folder from the Finder, and double click the OpenMeca icon.
Note that, the application is not code signed. If you are using a recent version of OS X, you may need to desactivate Gatekeeper in the System Preferences.

# Installation on windows
Simply download the installer [here](http://www.yakuru.fr/~openmeca/openmeca-v2_x86-setup.exe). 

# Installation on GNU/Linux
On a Debian based Linux distribution, simply run the following commands.
```
sudo apt-get install libqt5help5 libqt5svg5 libqt5opengl5 libqt5widgets5 libqt5gui5 libqt5xml5 libqt5core5a libboost-all-dev libqwt-qt5-6 libqglviewer2-qt5
cd /tmp/ && wget http://www.yakuru.fr/~openmeca/openmeca_2.x_amd64.deb 
sudo dpkg -i openmeca_2.x_amd64.deb
```
Now, you can lauch openmeca by typing `openmeca` in a terminal or by clicking the openmeca icon in the *Education* menu. To remove openmeca, simply enter the following command.
```
sudo dpkg -r openmeca
```


# The one minute install & compile guide for Linux
On a Debian based Linux distribution, simply enter the following terminal commands (and take a coffee).
```
sudo apt-get install build-essential git qtdeclarative5-dev qt5-default qttools5-dev-tools libqt5svg5-dev libqt5opengl5-dev qttools5-dev libboost-all-dev libqwt-headers libqwt-qt5-6 libqwt-qt5-dev libqglviewer-dev-qt5
git clone https://gitlab.com/damien.andre/openmeca.git
cd ./openmeca && make 
./SRC/OpenMeca/BUILD/openmeca
```
These commands do the following actions : 
 * install the required dependencies,
 * download the openmeca source package,
 * compile the source files with the `make` command and
 * run the executable file created by the compilation process.

Note that :
 * the compilation process can take several minutes.
 * you can install openmeca by typing `sudo make install`. By default openmeca is installed in the `/usr/local/bin/` directory.
 * if you want to uninstall openmeca, simply type `sudo make uninstall`.

## Technical information about source code ##

OpenMeca is written in C++ and is based on the [qt5](http://doc.qt.io/qt-5/), [qwt](http://qwt.sourceforge.net/), 
[qglviewer](http://libqglviewer.com/), [boost](http://www.boost.org/) and the [chronoengine](http://chronoengine.info/chronoengine/) C++ libraries. 
The graphical front-end is given thanks to the qt5, qglviewer and opengl libraries. 
The boost library is used to manage serialization (save and load procedures) 
and some boring stuffs (lexical_cast, bind, etc.). The chronoengine library is the physical engine of OpenMeca.

OpenMeca packages qglviewer, qwt, boost and chronoengine libraries. The chronoengine version used here is a custom version. 
So, you need to compile them before compiling openmeca. Note that openmeca is statically linked to these libraries. The aim of this design is to make 
easier the further deployment of openmeca. 

# Configuration files
The configuration files of openmeca are stored in the `$HOME/openmeca` directory.  

# Compilation
Simply type `make` in the `SRC` directory.

# License
OpenMeca is under the GPLv3 license. It is a free software, it means that you may copy, distribute and modify the software as 
long as you track changes/dates in source files. Any modifications to or software including (via compiler) 
GPL-licensed code must also be made available under the GPL along with build & install instructions.
See the LICENSE file for more information.

# Artwork
Most of the images and icons were extracted and inspired from the [openclipart](https://openclipart.org/) database. 

# Contact & always up-to-date information
All these information are available [here](https://gitlab.com/damien.andre/openmeca/raw/master/SRC/OpenMeca/Info.txt).


